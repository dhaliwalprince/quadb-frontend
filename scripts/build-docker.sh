#!/bin/bash
pushd `pwd`

cd deploy
echo "Docker build started"
docker build -t princedhaliwal/quadb-main-service .
echo "Docker build completed"
cd `popd`

