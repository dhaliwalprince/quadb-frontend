mkdir -p deploy
cp -r build/ deploy/
cp -r config deploy
cp -r templates deploy
cp quadb deploy
cp scripts/start.sh deploy
cp docker/Dockerfile deploy
cp docker/.dockerignore deploy
bash scripts/build-docker.sh
