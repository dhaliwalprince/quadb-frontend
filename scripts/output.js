const path = require('path');
const fs = require('fs')

const outputAssets = (stats, buildFolder) => stats.toJson()
    .assets.filter(asset => /\.(js|css)$/.test(asset.name))
    .map(asset => {
        return {
            folder: path.join(path.basename(buildFolder), path.dirname(asset.name)),
            name: path.basename(asset.name),
        };
    });

const writeJson = (assetFn, stats, buildFolder, outputFile) => {
    const assets = assetFn(stats, buildFolder)
    fs.writeFileSync(outputFile, JSON.stringify(assets), { encoding: 'utf8' })
}

const output = (stats, buildFolder, outputFile) => writeJson(outputAssets, stats, buildFolder, outputFile);

module.exports = output;