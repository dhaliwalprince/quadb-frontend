import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import AdminApp from './AdminApp';

ReactDOM.render(<AdminApp />, document.getElementById('root'));
