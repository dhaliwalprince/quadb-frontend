import React, { Component, Fragment } from 'react';
import { withStyles } from 'material-ui/styles';
import BrowserRouter from 'react-router-dom/BrowserRouter';
import AdminAppBar from './components/AdminAppBar';
import { Route, Link } from 'react-router-dom'
import LoginPage from './components/LoginPage';
import SignUpPage from './components/SignUpPage';
import Logout from './containers/Logout';
import OrderManager from './containers/Admin/OrderManager';
import Commons from './subapps/Commons';
import AdminRequestsContainer from './subapps/AdminRequestsContainer';
import UserManager from './containers/Admin/UserManager';
import ConfigurationList from './containers/Admin/ConfigurationList';
import MetaManager from './containers/Admin/MetaManager';
import withRouter from 'react-router/withRouter';
import ListItem from 'material-ui/List/ListItem';
import ListSubheader from 'material-ui/List/ListSubheader';
import { AuthorizedApi } from './globals/authorization';
import FileManager from './containers/Admin/FileManager';
import QuotationGenerator from './containers/Admin/QuotationGenerator';
import BlogEditor from './containers/Admin/BlogEditor';

class CAdminHome extends Component {
    componentDidMount() {
        this.props.history.push('/login');
    }

    render() {
        const { classes } = this.props;
        return (
            <aside className={classes.aside}>
                <div className={classes.left}>
                    <ListSubheader>
                        Basic Actions
                    </ListSubheader>
                    <ListItem button className={classes.button} component={Link} to='/orders'>
                        Orders
                    </ListItem>
                    <ListItem button className={classes.button} component={Link} to='/users'>
                        Users
                    </ListItem>
                    <ListItem button className={classes.button} component={Link} to='/website'>
                        Website
                    </ListItem>
                    <ListItem button className={classes.button} component={Link} to='/order-requests'>
                        Requests
                    </ListItem>
                    <ListItem button className={classes.button} component={Link} to='/file-manager'>
                        File Manager
                    </ListItem>
                </div>
                <ListSubheader>
                    Configuration options
                </ListSubheader>
                <ConfigurationList />
            </aside>
        )
    }
}

const styles = theme => ({
    padding: {
        width: '100%',
        height: theme.spacing.unit * 2,
    },
    root: {
        padding: theme.spacing.unit * 2,
        display: 'flex',
        flexDirection: 'row',
    },
    aside: {
        flex: 2,
        maxWidth: 400,
        borderRight: '1px solid lightgray'
    },
})

const AdminHome = withStyles(styles)(CAdminHome)

const Routes = withRouter(({ classes, match }) => {
    return (
        <Fragment>
            {
            <Fragment>
                <div className={classes.padding} />
                <main className={classes.root}>
                    <Route path="/" component={AdminHome} />
                    <Route path={"/login"} component={LoginPage} />
                    <Route path={"/logout"} component={Logout} />
                    <Route path={'/orders'} component={OrderManager} />
                    <Route path={'/users'} component={UserManager} />
                    <Route path={'/order-requests'} component={AdminRequestsContainer} />
                    <Route path={'/manage/meta/:metaId'} component={MetaManager} />
                    <Route path={'/file-manager'} component={FileManager} />
                    <Route path={'/manage/quotations'} component={QuotationGenerator} />
                    <Route path="/manage/blogs/new" component={BlogEditor} />
                </main>
                {
                    console.log(match) && match.location.pathname === '/login' || <Commons />
                }
            </Fragment>
            }
        </Fragment>
    )
})

class AdminApp extends Component {
    render() {
        const { classes } = this.props;

        return (
            <BrowserRouter>
                <Routes classes={classes} />
            </BrowserRouter>
        )
    }
}

export default withStyles(styles)(AdminApp);
