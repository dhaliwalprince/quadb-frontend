import React, {Component, Fragment} from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import MaterialAppBar from './components/AppBar'
import { withStyles } from 'material-ui/styles'
import LoginPage from './components/LoginPage';
import SignUpPage from './components/SignUpPage';
import ProfileLoader from './containers/ProfileLoader';
import Logout from './containers/Logout';
import AddressManager from './containers/AddressManager';
import OrderContainer from './containers/OrderContainer';
import CartManager from './containers/CartManager';
import PlaceOrder from './containers/PlaceOrder';
import DesignContainer from './containers/DesignContainer';
import ClothContainer from './containers/ClothContainer';
import Footer from './components/Footer';
import Designer from './components/Designer';
import Checkout from './containers/Checkout';
import RequestsContainer from './containers/RequestsContainer';
import RequestManager from './containers/RequestManager';
import logo from './components/Logo.png'
import withRouter from 'react-router/withRouter';
import QuotationListContainer from './containers/QuotationList';
import ConfirmQuotation from './containers/ConfirmQuotation';
import FourOFour from './components/FourOFour';

const styles = theme => ({
    content: {
        backgroundColor: theme.palette.background.default,
        width: '100%',
        boxSizing: 'border-box'
    },
    requests: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    logo: {
        height: 128,
        margin: 'auto',
        display: 'block'
    }
})

class RegisterHeader extends Component {
    render() {
        const { classes } = this.props;
        return (
            <div className={classes.header}>
                <img src={logo} className={classes.logo} alt={'Quadb Creations'} />
                {
                    this.props.children}
            </div>
        )
    }
}

class CRoutes extends Component {
    render() {
        const { classes } = this.props;
        return (
            <Fragment>
            { window.location.pathname.includes('/login') || window.location.pathname.includes('/signup') ? "" : <MaterialAppBar /> }
            <main className={classes.content}>
                <Route path={"/login"} exact component={() => <RegisterHeader classes={classes}><LoginPage /></RegisterHeader>} />
                <Route path={"/profile"} exact component={ProfileLoader} />
                <Route path={"/signup"} component={() => <RegisterHeader classes={classes}><SignUpPage /></RegisterHeader>} />
                <Route path={"/logout"} component={Logout} />
                <Route path="/manage/addresses" component={AddressManager} />
                <Route path="/manage/requests" exact component={RequestsContainer} />
                <Route path="/manage/requests/:requestId" component={RequestManager} />
                <Route path="/manage/orders" component={OrderContainer} />
                <Route path="/manage/cart" component={CartManager} />
                <Route path="/manage/designs" component={DesignContainer} />
                <Route path="/orders/:id/place" component={PlaceOrder} />
                <Route path="/cloth-stand" component={ClothContainer} />
                <Route path="/designer" component={Designer} />
                <Route path="/checkout/:designId" component={Checkout} />
                <Route path="/requests/:requestId/quotations" component={QuotationListContainer} />
                <Route path="/processOrder/confirm_quotation/:quotationId" component={ConfirmQuotation} />
            </main>
{ window.location.pathname.startsWith('/login') || window.location.pathname.startsWith('/signup') ? "" : <Footer /> }
        </Fragment>
        )
    }
}

const Routes = withRouter(CRoutes);

class App extends Component {
  render() {
    const { classes } = this.props
    return (
        <Router>
            <Routes classes={classes} />
        </Router>
    );
  }
}

export default withStyles(styles)(App);
