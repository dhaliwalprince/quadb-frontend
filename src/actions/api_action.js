const CALL_API = 'CALL_API';
const RECIEVE_DATA = 'RECIEVE_DATA';
const CATCH_ERROR = 'CATCH_ERROR';

export class ApiRequestConfig {
    constructor(url, options) {
        this.url = url;
        this.options = options;
    }

    setOptions(options) {
        this.options = options;
        return this
    }

    setUrl(url) {
        this.url = url;
        return this;
    }
}

export function request(apiConfig) {
    return {
        type: CALL_API,
        apiConfig,
    }
}

export function receiveResponse(response) {
    return {
        type: RECIEVE_DATA,
        response,
    }
}

export function catchError(error) {
    return {
        type: CATCH_ERROR,
        error
    }
}
