const FETCH_FEATURED_PRODUCT = 'FETCH_FEATURED_PRODUCT';
const REFRESH_FEATURED_PRODUCT = 'REFRESH_FEATURED_PRODUCT';
const LOADED_FEATURED_PRODUCT = 'LOADED_FEATURED_PRODUCT';

function fetchFeaturedProducts() {
    return {
        type: FETCH_FEATURED_PRODUCT
    }
}

function setFeaturedProducts(products) {
    return {
        type: LOADED_FEATURED_PRODUCT,
        products,
    }
}

export { FETCH_FEATURED_PRODUCT, REFRESH_FEATURED_PRODUCT }
