export const ADD_ALERT = 'ADD_ALERT';
export const REMOVE_ALERT = 'REMOVE_ALERT';
export const ADD_WORK = 'ADD_WORK';
export const REMOVE_WORK = 'REMOVE_WORK';


export function addAlert(alert) {
    return {
        type: ADD_ALERT,
        alert
    }
}

export function removeAlert(alertId) {
    return {
        type: REMOVE_ALERT,
        alertId
    }
}

export function addWork(work) {
    return {
        type: ADD_WORK,
        work
    }
}

export function removeWork(workId) {
    return {
        type: REMOVE_WORK,
        workId
    }
}
