import React, { Component } from 'react';
import Dialog, { DialogTitle, DialogActions, DialogContent } from 'material-ui/Dialog';
import { withStyles } from 'material-ui/styles';
import AddressForm from '../containers/AddressForm';

const styles = theme => ({
    dialog: {
        width: window.innerWidth > 480 ? '480px' : window.innerWidth - theme.spacing.unit*2
    }
})

class AddNewAddressDialog extends Component {
    render() {
        const { classes, ...other } = this.props
        console.log(this.props)
        return (
            <Dialog {...other} classes={{ paper: classes.dialog}}>
                <DialogTitle>Add new address</DialogTitle>
                <AddressForm onRequestClose={this.props.onRequestClose}/>
            </Dialog>
        )
    }
}

export default withStyles(styles)(AddNewAddressDialog)
