import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import List, { ListItem, ListItemSecondaryAction, ListItemText } from 'material-ui/List';
import IconButton from 'material-ui/IconButton'
import DeleteIcon from 'material-ui-icons/Delete'
import Divider from 'material-ui/Divider'

const styles = theme => ({
    
})

class Address extends Component {
    render() {
        const { address, classes } = this.props

        return (
            <ListItem button>
                <ListItemText primary={address.toOneLine()} secondary={address.label} />
                <ListItemSecondaryAction>
                    <IconButton onClick={() => this.props.onDeleteAction(address)} aria-label="Delete">
                        <DeleteIcon />
                    </IconButton>
                </ListItemSecondaryAction>
            </ListItem>
        )
    }
}

class AddressList extends Component {
    render() {
        const { classes, addresses } = this.props;
        return (
            <List>
                {
                    addresses.map((address, index) => {
                        return (
                            <Address address={address} key={index} classes={classes} onDeleteAction={this.props.onDeleteAction} />
                        )
                    })
                }
            </List>
        )
    }
}

export default withStyles(styles)(AddressList);
