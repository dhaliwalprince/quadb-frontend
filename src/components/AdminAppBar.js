import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar/AppBar';
import Toolbar from 'material-ui/Toolbar/Toolbar';
import Button from 'material-ui/Button/Button';
import Link from 'react-router-dom/Link';

const styles = theme => ({
    toolbar: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    button: {
        color: 'white',
    },
    left: {
        display: 'flex',
        justifyContent: 'space-around'
    },
    search: {
        color: 'white',
        border: 'none',
        textDecoration: 'none',
        width: '40%',
        borderRadius: '2px',
    },
    input: {
        width: '100%',
        padding: theme.spacing.unit * 1.5,
        border: 'none',
        borderRadius: '2px',
        backgroundColor: '#6b7bd4'
    },
    spacer: theme.mixins.toolbar,
})

class AdminAppBar extends Component {
    render() {
        const { classes } = this.props;

        return (
            <div>
                <AppBar position='fixed' color='primary'>
                    <Toolbar className={classes.toolbar}>
                        <Button className={classes.button} component={Link} to='/'>
                            <span style={{fontWeight: 'bold'}}>Quadb</span> Creations
                        </Button>
                        <div className={classes.search}>
                            { /* <input className={classes.input} type={'search'} onChange={this.handleChange} placeholder='search' />
                        */}</div>

                    </Toolbar>
                </AppBar>
                <div className={classes.spacer} />
            </div>
        )
    }
}

export default withStyles(styles)(AdminAppBar)
