import React, { Component } from 'react';

const styles = theme => ({

})

class AdminHome extends Component {
    render() {
        const { classes } = this.props;

        return (
            <div className={classes.left}>
                <Button className={classes.button} component={Link} to='/orders'>
                    Orders
                </Button>
                <Button className={classes.button} component={Link} to='/users'>
                    Users
                </Button>
                <Button className={classes.button} component={Link} to='/website'>
                    Website
                </Button>
                <Button className={classes.button} component={Link} to='/order-requests'>
                    Requests
                </Button>
            </div>  
        )
    }
}
