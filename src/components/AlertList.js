import React, { Component } from 'react';
import withStyles from 'material-ui/styles/withStyles';
import Button from 'material-ui/Button/Button';

const styles = theme => ({

})

class CAlert extends Component {
    render() {
        const { classes, alert, onDismiss } = this.props;

        return (
            <div className={classes.alert}>
                <div className={classes.alertHeading}>
                    <b>{alert.title}</b>
                </div>
                <div className={classes.alertDescription}>
                    <p>{alert.message}</p>
                </div>
                <div className={classes.alertActions}>
                    <Button onClick={onDismiss}>
                        [x]
                    </Button>
                </div>
            </div>
        )
    }
}

const Alert = withStyles(styles)(CAlert);

class AlertList extends Component {
    render() {
        const { classes, alerts, onDismiss } = this.props;
        return (
            <div className={classes.alerts}>
                { alerts.map((alert, index) => (
                    <Alert alert={alert} key={index} onDismiss={() => onDismiss(index)}/>
                ))}
            </div>
        )
    }
}

export default withStyles(styles)(AlertList)
