import React, { Component } from 'react';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui-icons/Menu';
import AccountCircle from 'material-ui-icons/AccountCircle'
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import logo from './quadb-logo.png';
import ProfileMenu from './ProfileMenu';
import { withStyles } from 'material-ui/styles';
import { withRouter } from 'react-router-dom';
import AppDrawer, { AppBarOptions } from './AppDrawer';
import { AuthorizedApi } from '../globals/authorization';
import LoginOrSignupOption from './LoginOrSignupOption';
import Typography from 'material-ui/Typography/Typography';
import ExtraHeaderInfo from './ExtraHeaderInfo';
import { Hidden } from 'material-ui';

const styles = theme => ({
    logoImage: {
        display: 'block',
        margin: 'auto',
        height: 48
    },
    menuButton: {
        marginLeft: -12,
    },
    toolbar: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: theme.spacing.unit * 2,
    },
    toolbarDesk: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    logo: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'row',
    },
    logoText: {
        verticalAlign: 'middle',
    },
    rootAppbar: {
        boxShadow: 'none',
    },
    position: {
        position: 'normal',
    },
    flex: {
        display: 'flex',
        alignItems: 'center',
        verticalAlign: 'middle',
    }
});

function QuadbLogo({ classes, history }) {
    return (
        <div className={classes.logo}>
            <Button component="a" href="/" className={classes.logo}>
                <img className={classes.logoImage} src={logo} alt={'QuadB'} />
            </Button>
            <Typography className={classes.logoText} type='button'>
            <b>QUADB</b> Creations
            </Typography>
        </div>
    )
}

const ProfileMenuButton = withStyles(styles)(({ classes, open, onClick }) => {
    return !AuthorizedApi.authApi.loggedIn ? <LoginOrSignupOption short /> :
        <IconButton onClick={onClick} aria-haspopup="true" aria-owns={open ? ProfileMenu.ID : null}>
            <AccountCircle />
        </IconButton>
})

class MaterialAppBar extends Component {
    state = {
        open: false,
        openDrawer: false
    };

    handleProfileClick = (event) => {
        this.setState({ open: true, anchorEl: event.currentTarget })
    }

    handleRequestClose = () => {
        this.setState({ anchorEl: null, open: false })
    }

    handleDrawer = () => {
        this.setState({ openDrawer: !this.state.openDrawer })
    }

    render() {
        const { classes } = this.props 
        const { open, anchorEl } = this.state;
        return (
            <header>
                <AppBar classes={{ root: classes.rootAppbar, positionFixed: classes.position }} position={'static'} color={'white'}>
                    <ExtraHeaderInfo />
                    <Hidden smDown implementation={'css'}>
                        <Toolbar className={classes.toolbarDesk}>
                            <div className={classes.gridItem}>
                                <QuadbLogo classes={classes} history={this.props.history} />
                            </div>
                            <div className={classes.gridItem + ' ' + classes.flex}>
                                <AppBarOptions horizontal />
                                <ProfileMenuButton open={open} onClick={this.handleProfileClick} />
                            </div>
                        </Toolbar>
                    </Hidden>
                    <Hidden smUp implementation={'css'}>
                        <Toolbar className={classes.toolbar}>
                            <IconButton className={classes.menuButton} aria-label="Menu" onClick={this.handleDrawer}>
                                <MenuIcon />
                            </IconButton>
                            <QuadbLogo classes={classes} history={this.props.history} />
                            <ProfileMenuButton open={open} onClick={this.handleProfileClick} />
                        </Toolbar>
                    </Hidden>
                </AppBar>
                <ProfileMenu anchorEl={anchorEl} open={open} onRequestClose={this.handleRequestClose} />
                <AppDrawer open={this.state.openDrawer} onClose={this.handleDrawer}/>
            </header>
        )
    }
}

export default withStyles(styles)(withRouter(MaterialAppBar))
