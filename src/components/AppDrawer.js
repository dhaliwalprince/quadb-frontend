import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import Drawer from 'material-ui/Drawer/Drawer';
import List from 'material-ui/List/List';
import ListItem from 'material-ui/List/ListItem';
import ListItemText from 'material-ui/List/ListItemText';
import withRouter from 'react-router-dom/withRouter';
import Link from './Link';

const styles = theme => ({
    root: {
        width: 480,
    },
    drawerHeader: theme.mixins.toolbar,
    drawerPaper: {
        width: 360
    },
    horizontal: {
    },
    text: {
        textTransform: 'uppercase',
        fontSize: 14,
        letterSpacing: '0.1em',
        fontWeight: 'bold',
        color: 'black',
        padding: theme.spacing.unit,
    },
    horizontalItem: {
        display: 'inline-block',
        margin: theme.spacing.unit * 2,
    }
})

const urls = [
    {
        name: 'Home',
        url: '/',
    },
    {
        name: 'About Us',
        url: '/about-us',
    },
    {
        name: 'Designer',
        url: '/designer',
    },
    {
        name: 'Our Clients',
        url: '/clients',
    },
    {
        name: 'Blog',
        url: '/blog'
    },
    {
        name: 'Contact',
        url: '/contact',
    }
]

const AppBarOptionsC = ({classes, horizontal }) => (
    <List className={horizontal && classes.horizontal}>
    {
        urls.map((url, index) => (
            <ListItem disableGutters className={horizontal && classes.horizontalItem} key={index} component={Link} to={url.url}>
                <ListItemText disableTypography className={classes.text} primary={url.name} />
            </ListItem>
        ))
    }
    </List>
)

const AppBarOptions = withStyles(styles)(AppBarOptionsC);

export { AppBarOptions }

class AppDrawer extends Component {

    handleClick = url => {
        this.props.history.push(url.url)
        this.props.onClose()
    }

    render() {
        const { classes } = this.props;

        return (
            <div className={classes.root}>
                <Drawer open={this.props.open} onClose={this.props.onClose} classes={{ paper: classes.drawerPaper }}>
                    <div
                        tabIndex={0}
                        role="button"
                        onClick={this.props.onClose}
                        onKeyDown={this.props.onClose}
                    >
                        <div className={classes.drawerHeader} /> 
                        <div className={classes.list}>
                            <AppBarOptions />
                        </div>
                    </div>
                </Drawer>
            </div>
        )
    }
}

export default withRouter(withStyles(styles)(AppDrawer));
