import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import Typography from 'material-ui/Typography'
import OrderDetails from '../components/OrderDetails';
import Paper from 'material-ui/Paper';
import { DialogActions } from 'material-ui/Dialog'
import Button from 'material-ui/Button/Button';

const styles = theme => ({
    root: {
        maxWidth: 480,
        display: 'block',
        margin: 'auto',
    },
    paper: {
        padding: theme.spacing.unit,
        marginBottom: theme.spacing.unit * 2
    },
    details: {
        padding: theme.spacing.unit
    }
})

class CartItem extends Component {
    render() {
        const { index, classes, order, onEdit, onRemove, onPlace } = this.props;
        return (
            <Paper className={classes.paper}>
                <div className={classes.details}>
                    <Typography type={'caption'}>Item: {index + 1}</Typography>
                    <OrderDetails details={order.order_details} />
                    <DialogActions>
                        <Button color={'red'} onClick={onRemove}>
                            Remove
                        </Button>
                        <Button onClick={onEdit}>
                            Edit
                        </Button>
                        <Button onClick={onPlace}>
                            Place Order for this item
                        </Button>
                    </DialogActions>
                </div>
            </Paper>
        )
    }
}

class Cart extends Component {
    render() {
        const { classes, cart, onEdit, onPlace, onRemove } = this.props;

        return (
            <div className={classes.root}>
                {
                    cart.map((order, index) => {
                        return (
                            <CartItem classes={classes} key={index} index={index}
                                    order={order}
                                    onEdit={() => onEdit(order)}
                                    onPlace={() => onPlace(order)}
                                    onRemove={() => onRemove(order)}
                            />
                        )
                    })
                }
            </div>
        )
    }
}

export default withStyles(styles)(Cart)