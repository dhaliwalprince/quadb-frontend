import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper/Paper';
import Typography from 'material-ui/Typography/Typography';
import Divider from 'material-ui/Divider/Divider';
import clothStand from './cloth-stand-cover.png'

const styles = theme => ({
    root: {
        maxWidth: 800,
        margin: 'auto',
    },
    headline: {
        lineHeight: '1.5em'
    },
    headlineWrapper: {
        backgroundColor: '#6c99d0',
        padding: theme.spacing.unit * 2,
        color: 'white',
        borderRadius: 0,
    },
    divider: {
        marginTop: theme.spacing.unit * 2,
        marginBottom: theme.spacing.unit * 2
    },
    coverPhoto: {
        background: 'url(' + clothStand + ') center center /cover',
        padding: theme.spacing.unit * 4,
        borderRadius: 0,
    },
    topHeading: {
        color: 'white',
        borderRadius: theme.spacing.unit,
    },
    cloth: {
        marginTop: theme.spacing.unit,
        maxWidth: 360,
        marginBottom: theme.spacing.unit * 2,
        padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px ${theme.spacing.unit * 2}px ${theme.spacing.unit * 2}px`
    },
    clothTitle: {
        marginTop: theme.spacing.unit,
        marginBottom: theme.spacing.unit,
        fontSize: theme.spacing.unit * 2
    },
    clothes: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-around'
    }
})

class Cloth extends Component {
    render() {
        const { classes, cloth } = this.props;
        return (
            <Paper className={classes.cloth} elevation={1}>
                <Typography type={'button'} align='center' className={classes.clothTitle}>
                    {cloth.name.toUpperCase()}
                </Typography>
                <Typography type='' align='center'>
                    {cloth.features}
                </Typography>
            </Paper>
        )
    }
}

class ClothStand extends Component {
    render() {
        const { classes, clothes } = this.props;
        return (
            <div className={classes.root}>
                <Paper className={classes.root}>
                    <Paper className={classes.coverPhoto} elevation={0}>
                        <div className={classes.backCover}>
                            <Typography type='headline' className={classes.topHeading} align='center'>
                                CLOTH STAND
                            </Typography>
                        </div>
                    </Paper>
                    <Paper className={classes.headlineWrapper} elevation={0}>
                        <Typography type='body' align='center' className={classes.headline}>
                            We at QuadB, believe in giving maximum options to customers in printing, product design and cloth quality. Based on different requirements of our customers, we provide 9 different types of cloth and out of them, 5 are premium quality clothes.
                        </Typography>
                    </Paper>
                </Paper>
                <Divider className={classes.divider} />

                <div className={classes.clothes}>
                {
                    clothes.map((cloth, index) => (
                        <Cloth key={index} cloth={cloth} classes={classes} />
                    ))
                }
                </div>
            </div>
        )
    }
}

export default withStyles(styles)(ClothStand)
