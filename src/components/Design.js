import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import Card from 'material-ui/Card/Card';
import CardMedia from 'material-ui/Card/CardMedia';
import CardContent from 'material-ui/Card/CardContent';
import Typography from 'material-ui/Typography/Typography';
import CardActions from 'material-ui/Card/CardActions';
import Button from 'material-ui/Button/Button';
import Link from 'react-router-dom/Link';
import { AuthorizedApi } from '../globals/authorization';
import Loader from './Loader';
import Error from './Error';
import withRouter from 'react-router-dom/withRouter';

const styles = theme => ({
    root: {
        margin: theme.spacing.unit,
        width: '800px'
    },
    media: {
        minHeight: '800px'
    }
})



class Design extends Component {
    state = {
        loading: true,
    }

    componentDidMount() {
        const headers = new Headers()
        headers.set('Authorization', AuthorizedApi.authApi.getToken())
        fetch('/api/assets/images/private/' + this.props.design.image_url, { headers })
            .then(res => res.blob() || res.json())
            .then(blob => {
                const url = URL.createObjectURL(blob)
                this.setState({ image: url, loading: false })
            })
            .catch(err => {
                this.setState({ error: err, loading: false })
            })
    }

    handleCheckout = () => {
        this.props.history.push('/checkout/'+this.props.design.image_url.slice(0, -4))
    }

    render() {
        const { classes, design, onRemove } = this.props
        return (
            <Card className={classes.root}>
                { this.state.loading || <CardMedia
                    className={classes.media}
                    component="img"
                    src={this.state.image}
                    title={design.name}
                /> }
                {
                    this.state.loading && <Loader classes={classes} />
                }
                {
                    this.state.error && <Error classes={classes.error} error={this.state.error} />
                }
                <CardContent>
                    <Typography type='headline'>
                        { design.name }
                    </Typography>
                </CardContent>
                <CardActions>
                    <Button color='primary' onClick={this.handleCheckout}>
                        Checkout
                    </Button>
                <Button color='primary' onClick={onRemove}>
                    Delete
                </Button>
                </CardActions>
            </Card>
        )
    }
}

class AddDesignComp extends Component {
    render() {
        const { classes, onAdd } = this.props
        return (
            <Card className={classes.root}>
                <CardContent>
                    <Typography type='headline'>
                        Create new design
                    </Typography>
                </CardContent>
                <CardActions>
                    <Button component={Link} to='/designer' color='primary' onClick={onAdd}>
                    Create
                    </Button>
                </CardActions>
            </Card>
        )
    }
}

export let AddDesign = withStyles(styles)(AddDesignComp)

export default withRouter(withStyles(styles)(Design))
