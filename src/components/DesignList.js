import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import Design, { AddDesign } from './Design';

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    }
})

class DesignList extends Component {
    render() {
        const { classes, designs } = this.props;
        return (
            <div className={classes.root}>
                {
                    designs.map(design => (
                        <Design design={design} key={design.id} onRemove={() => this.props.onRemove(design)} />
                    ))
                }
                <AddDesign />
            </div>
        )
    }
}

export default withStyles(styles)(DesignList);
