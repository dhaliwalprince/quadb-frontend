import React, { Component } from 'react';
import withStyles from 'material-ui/styles/withStyles';
import { AuthorizedApi } from '../globals/authorization';
import Loader from './Loader';
import Error from './Error';
import Typography from 'material-ui/Typography/Typography';
import SecureImage from './SecureImage';

const styles = theme => ({
    image: {
        height: '480px'
    }
})

export function designUrlFor(design, options) {
    const headers = new Headers();
    headers.set('Authorization', `Bearer ${AuthorizedApi.authApi.getToken()}`)
    return fetch('/api/assets/images/private/'+design.image_url, { headers }).then(res => {
        if (!res.ok) {
            return options.onFailed({ message: 'Unknown error occurred' })
        }

        let url = res.headers.get('location')

        url && options.onSuccess(url);
    }, error => options.onFailed(error));
}

class DesignPreview extends Component {
    state = {
        url: '',
        error: '',
        loading: true
    }

    onSuccess = url => {
        this.setState({ loading: false, url })
    }

    onFailed = error => {
        this.setState({ error, loading: false })
    }

    componentDidMount() {
        designUrlFor(this.props.design, this)
    }

    componentDidUpdate(oldProps) {
        if (this.props.design.id !== oldProps.design.id) {
            designUrlFor(this.props.design, this)
        }
    }

    componentWillReceiveProps(newProps) {
        if (newProps.design.id !== this.props.design.id) {
            this.setState({ error: false, loading: true })
        }
    }
    render() {
        const { classes, design } = this.props;

        return (
            <div className={classes.root}>
                <SecureImage image_url={design.image_url} />
                <Typography type='caption' align='center'>
                {design.name}
                </Typography>
            </div>
        )
    }
}

export default withStyles(styles)(DesignPreview);