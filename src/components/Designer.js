import React, { Component } from 'react';
import withStyles from 'material-ui/styles/withStyles';
import Button from 'material-ui/Button/Button';
import { AuthorizedApi } from '../globals/authorization';
import Paper from 'material-ui/Paper/Paper';
import Loader from './Loader';
import Input from 'material-ui/Input/Input';
import InputLabel from 'material-ui/Input/InputLabel';
import FormControl from 'material-ui/Form/FormControl';
import FormHelperText from 'material-ui/Form/FormHelperText';
import Typography from 'material-ui/Typography/Typography';
import Link from 'react-router-dom/Link';

const styles = theme => ({
    root: {
        display: 'flex',
        justifyContent: 'space-around',
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    control: {
        minWidth: '480px',
        padding: theme.spacing.unit * 2,
    },
    progress: {
        height: theme.spacing.unit * 2,
    },
    saveProgress: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: theme.spacing.unit
    },
    formControl: {
        width: '100%',
        marginTop: theme.spacing.unit,
    },
    saveInfo: {
        display: 'flex',
        verticalAlign: 'middle',
        alignItems: 'center',
        color: 'gray',
    },
    checkoutButton: {
        marginTop: theme.spacing.unit * 2,
    }
})

class Designer extends Component {
    state = {
        blob: '',
        name: '',
        saving: false,
        saved: false,
        design: {
            hash: 'helods-sdf-s2-df-sdf-asdf.png',
            id: -1,
        }
    }
    componentDidMount() {
        var $yourDesigner = window.$(this.refs.designer),
            pluginOpts = {
                stageWidth: 1200,
                editorMode: false,
                fonts: [
                    { name: 'Helvetica' },
                    { name: 'Times New Roman' },
                    { name: 'Pacifico', url: 'Enter_URL_To_Pacifico' },
                    { name: 'Arial' },
                    { name: 'Lobster', url: 'google' }
                ],
                customTextParameters: {
                    colors: false,
                    removable: true,
                    resizable: true,
                    draggable: true,
                    rotatable: true,
                    autoCenter: true,
                    boundingBox: "Base"
                },
                customImageParameters: {
                    draggable: true,
                    removable: true,
                    resizable: true,
                    rotatable: true,
                    colors: '#000',
                    autoCenter: true,
                    boundingBox: "Base"
                },
                actions: {
                    'top': ['download', 'print', 'snap', 'preview-lightbox'],
                    'right': ['magnify-glass', 'zoom', 'reset-product', 'qr-code', 'ruler'],
                    'bottom': ['undo', 'redo'],
                    'left': ['manage-layers', 'info']
                }
            };

        this.yourDesigner = new window.FancyProductDesigner($yourDesigner, pluginOpts);

        $yourDesigner.on('elementModify', (event, element, parameters) => {
            this.setState({ saved: false })
        })
    }

    handleSave = () => {
        if (!this.state.name || this.state.name.length === 0) {
            this.setState({ error: { message: 'Name cannot be empty.' } })
            return
        }
        this.yourDesigner.getProductDataURL(data => {
            AuthorizedApi.authApi.makeRequest('/api/assets/images/private', {
                method: 'POST',
                body: JSON.stringify({
                    base64_image: data,
                    name: this.state.name
                })
            }, {
                    onSuccess: res => {
                        this.setState({ design: res, saving: false, error: false, saved: true })
                    },
                    onFailed: err => {
                        this.setState({ saving: false, error: err })
                    }
                })
        });
        this.setState({ saving: true, error: false })
    }

    handleChange = ({ target }) => {
        this.setState({ name: target.value, saved: false })
    }

    render() {
        const { classes } = this.props;
        return (
            <div className={classes.root}>
                <div ref="designer" className={"fpd-container fpd-shadow-2 fpd-topbar fpd-tabs fpd-tabs-side fpd-top-actions-centered fpd-bottom-actions-centered fpd-views-inside-left " + classes.designer}>
                    <div className="fpd-product" title="Shirt Front" data-thumbnail="images/yellow_shirt/front/preview.png">
                        <img src="images/yellow_shirt/front/base.png" title="Base" data-parameters='{"left": 325, "top": 329, "colors": "#d59211", "price": 20, "colorLinkGroup": "Base"}' />
                        <img src="images/yellow_shirt/front/shadows.png" title="Shadow" data-parameters='{"left": 325, "top": 329}' />
                        <img src="images/yellow_shirt/front/body.png" title="Hightlights" data-parameters='{"left": 322, "top": 137}' />
                        <span title="Any Text" data-parameters='{"boundingBox": "Base", "left": 326, "top": 232, "zChangeable": true, "removable": true, "draggable": true, "rotatable": true, "resizable": true, "colors": "#000000"}' >Default Text</span>
                        <div className="fpd-product" title="Shirt Back" data-thumbnail="images/yellow_shirt/back/preview.png">
                            <img src="images/yellow_shirt/back/base.png" title="Base" data-parameters='{"left": 317, "top": 329, "colorLinkGroup": "Base", "price": 20}' />
                            <img src="images/yellow_shirt/back/body.png" title="Hightlights" data-parameters='{"left": 333, "top": 119}' />
                            <img src="images/yellow_shirt/back/shadows.png" title="Shadow" data-parameters='{"left": 318, "top": 329}' />
                        </div>
                    </div>
                    <div className="fpd-product" title="Sweater" data-thumbnail="images/sweater/preview.png">
                        <img src="images/sweater/basic.png" title="Base" data-parameters='{"left": 332, "top": 311, "colors": "#D5D5D5,#990000,#cccccc", "price": 20}' />
                        <img src="images/sweater/highlights.png" title="Hightlights" data-parameters='{"left": 332, "top": 311}' />
                        <img src="images/sweater/shadow.png" title="Shadow" data-parameters='{"left": 332, "top": 309}' />
                    </div>
                    <div className="fpd-product" title="Scoop Tee" data-thumbnail="images/scoop_tee/preview.png">
                        <img src="images/scoop_tee/basic.png" title="Base" data-parameters='{"left": 314, "top": 323, "colors": "#98937f, #000000, #ffffff", "price": 15}' />
                        <img src="images/scoop_tee/highlights.png" title="Hightlights" data-parameters='{"left":308, "top": 316}' />
                        <img src="images/scoop_tee/shadows.png" title="Shadow" data-parameters='{"left": 308, "top": 316}' />
                        <img src="images/scoop_tee/label.png" title="Label" data-parameters='{"left": 314, "top": 140}' />
                    </div>
                    <div className="fpd-product" title="Hoodie" data-thumbnail="images/hoodie/preview.png">
                        <img src="images/hoodie/basic.png" title="Base" data-parameters='{"left": 313, "top": 322, "colors": "#850b0b", "price": 40}' />
                        <img src="images/hoodie/highlights.png" title="Hightlights" data-parameters='{"left": 311, "top": 318}' />
                        <img src="images/hoodie/shadows.png" title="Shadow" data-parameters='{"left": 311, "top": 321}' />
                        <img src="images/hoodie/zip.png" title="Zip" data-parameters='{"left": 303, "top": 216}' />
                    </div>
                    <div className="fpd-product" title="Shirt" data-thumbnail="images/shirt/preview.png">
                        <img src="images/shirt/basic.png" title="Base" data-parameters='{"left": 327, "top": 313, "colors": "#6ebed5", "price": 10}' />
                        <img src="images/shirt/collar_arms.png" title="Collars & Arms" data-parameters='{"left": 326, "top": 217, "colors": "#000000"}' />
                        <img src="images/shirt/highlights.png" title="Hightlights" data-parameters='{"left": 330, "top": 313}' />
                        <img src="images/shirt/shadow.png" title="Shadow" data-parameters='{"left": 327, "top": 312}' />
                        <span title="Any Text" data-parameters='{"boundingBox": "Base", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "colors": "#000000"}' >Default Text</span>
                    </div>
                    <div className="fpd-product" title="Short" data-thumbnail="images/shorts/preview.png">
                        <img src="images/shorts/basic.png" title="Base" data-parameters='{"left": 317, "top": 332, "colors": "#81b5eb", "price": 15}' />
                        <img src="images/shorts/highlights.png" title="Hightlights" data-parameters='{"left": 318, "top": 331}' />
                        <img src="images/shorts/pullstrings.png" title="Pullstrings" data-parameters='{"left": 307, "top": 195, "colors": "#ffffff"}' />
                        <img src="images/shorts/midtones.png" title="Midtones" data-parameters='{"left": 317, "top": 332}' />
                        <img src="images/shorts/shadows.png" title="Shadow" data-parameters='{"left": 320, "top": 331}' />
                    </div>
                    <div className="fpd-product" title="Basecap" data-thumbnail="images/cap/preview.png">
                        <img src="images/cap/basic.png" title="Base" data-parameters='{"left": 318, "top": 311, "colors": "#ededed", "price": 5}' />
                        <img src="images/cap/highlights.png" title="Hightlights" data-parameters='{"left": 309, "top": 300}' />
                        <img src="images/cap/shadows.png" title="Shadows" data-parameters='{"left": 306, "top": 299}' />
                    </div>
                    <div className="fpd-design">
                        <div className="fpd-category" title="Swirls">
                            <img src="images/designs/swirl.png" title="Swirl" data-parameters='{"zChangeable": true, "left": 215, "top": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "price": 10, "boundingBox": "Base", "autoCenter": true}' />
                            <img src="images/designs/swirl2.png" title="Swirl 2" data-parameters='{"left": 215, "top": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "price": 5, "boundingBox": "Base", "autoCenter": true}' />
                            <img src="images/designs/swirl3.png" title="Swirl 3" data-parameters='{"left": 215, "top": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "autoCenter": true}' />
                            <img src="images/designs/heart_blur.png" title="Heart Blur" data-parameters='{"left": 215, "top": 200, "colors": "#bf0200", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "price": 5, "boundingBox": "Base", "autoCenter": true}' />
                            <img src="images/designs/converse.png" title="Converse" data-parameters='{"left": 215, "top": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "autoCenter": true}' />
                            <img src="images/designs/crown.png" title="Crown" data-parameters='{"left": 215, "top": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "autoCenter": true}' />
                            <img src="images/designs/men_women.png" title="Men hits Women" data-parameters='{"left": 215, "top": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "boundingBox": "Base", "autoCenter": true}' />
                        </div>
                        <div className="fpd-category" title="Retro">
                            <img src="images/designs/retro_1.png" title="Retro One" data-parameters='{"left": 210, "top": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "scale": 0.25, "price": 7, "boundingBox": "Base", "autoCenter": true}' />
                            <img src="images/designs/retro_2.png" title="Retro Two" data-parameters='{"left": 193, "top": 180, "colors": "#ffffff", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "scale": 0.46, "boundingBox": "Base", "autoCenter": true}' />
                            <img src="images/designs/retro_3.png" title="Retro Three" data-parameters='{"left": 240, "top": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "scale": 0.25, "price": 8, "boundingBox": "Base", "autoCenter": true}' />
                            <img src="images/designs/heart_circle.png" title="Heart Circle" data-parameters='{"left": 240, "top": 200, "colors": "#007D41", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "scale": 0.4, "boundingBox": "Base", "autoCenter": true}' />
                            <img src="images/designs/swirl.png" title="Swirl" data-parameters='{"left": 215, "top": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "price": 10, "boundingBox": "Base", "autoCenter": true}' />
                            <img src="images/designs/swirl2.png" title="Swirl 2" data-parameters='{"left": 215, "top": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "price": 5, "boundingBox": "Base", "autoCenter": true}' />
                            <img src="images/designs/swirl3.png" title="Swirl 3" data-parameters='{"left": 215, "top": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true}' />
                        </div>
                    </div>
                </div>
                <Paper className={classes.control}>
                    <Typography type='headline'>
                        Design your Tee
                    </Typography>
                    <div className={classes.designName}>
                        <FormControl disabled={this.state.saving} className={classes.formControl}>
                            <InputLabel htmlFor="name">Name</InputLabel>
                            <Input type="text" name="name" onChange={this.handleChange} className={classes.input} />
                            <FormHelperText>{this.state.error && this.state.error.message}</FormHelperText>
                        </FormControl>
                    </div>
                    <div className={classes.saveProgress}>
                        <Button disabled={this.state.saving || this.state.saved} raised color='primary' className={classes.saveButton} onClick={this.handleSave}>
                            Save
                        </Button>
                        {this.state.saving && <Loader classes={classes} />}
                        {this.state.saved && <FormHelperText className={classes.saveInfo}>{this.state.design.hash}</FormHelperText>}
                    </div>
                    <div className={classes.saveProgress}>
                        <Button disabled={!this.state.saved} raised color='primary' component={Link} to={ '/checkout/' + this.state.design.hash }>
                            checkout
                        </Button>
                        <FormHelperText className={classes.saveInfo}>{!this.state.saved && 'Please save the design before checkout.'}</FormHelperText>
                    </div>
                    <div className={classes.faltuActions}>
                        <div className={classes.socialButtons}>
                        </div>
                    </div>
                </Paper>
            </div>
        )
    }
}

export default withStyles(styles)(Designer);
