import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import Dialog, { DialogActions } from 'material-ui/Dialog';
import Button from 'material-ui/Button/Button';

const styles = theme => ({
    errorWrapper: {
        width: '100%',
        display: 'flex',
        margin: 'auto',
        maxWidth: '480px'
    }
})

function reduceError(err) {
    let error = err;
    if (error instanceof SyntaxError) {
        error = { message: 'Unexpected error occurred.' }
    } else if (error.message.startsWith('Unexpected token')) {
        error = { message: 'Unexpected error occurred.' }
    }

    return error
}

class Error extends Component {
    render() {
        let { classes, error } = this.props;
        error = reduceError(error)
        return (
            <div className={classes.errorWrapper}>
                <div className={classes.error}>{error.message}</div>
            </div>
        )
    }
}

class ShortErrorComponent extends Component {
    state = {
        open: true,
    }

    handleClose = () => {
        this.setState({ open: false })
        this.props.onClose && this.props.onClose()
    }
    
    render() {
        let { classes, error } = this.props;

        error = reduceError(error)
        
        return (
            <Dialog open={this.state.open} className={classes.errorWrapper}>
                <div className={classes.error}>{error.message}</div>
                <DialogActions>
                    <Button onClick={this.handleClose}>
                    Close
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }
}

export let ShortError = withStyles(styles)(ShortErrorComponent);

export default withStyles(styles)(Error)
