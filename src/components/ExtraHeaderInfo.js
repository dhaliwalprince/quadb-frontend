import React, { Component } from 'react';
import { withStyles } from 'material-ui';
import fb from './icons/social-facebook-circular-button.svg';
const styles = theme => ({
    socialLink: {
        margin: theme.spacing.unit,
    },
    logo: {
        height: theme.spacing.unit * 4,
    },
    root: {
        backgroundColor: '#00e5ff',
        margin: 'none',
        color: 'white',
        fontFamily: 'Roboto',
    },
    blueBackground: {
        display: 'flex',
        alignItems: 'center',
        verticalAlign: 'middle',
        justifyContent: 'space-between',
    },
    limitWidth: {
        maxWidth: 800,
        display: 'block',
        margin: 'auto',
    },
    flex: {
        display: 'flex',
    },
    column: {
        padding: theme.spacing.unit,
    },
    border: {
        borderLeft: '1px solid white',
        borderRight: '1px solid white',
    }
})

class ExtraHeaderInfo extends Component {
    state = {
        user: {}
    }

    render() {
        const { classes } = this.props;
        return (
            <div className={classes.root}>
                <div className={classes.limitWidth}>
                    <div className={classes.blueBackground}>
                        <div className={classes.flex}>
                            <div className={classes.column}>
                                Welcome { this.state.user.name ? this.state.user.name : '' }
                            </div>
                            <div className={classes.column + ' ' + classes.border}>
                            Call us +91-72062-46045
                            </div>
                            <div className={classes.column}>
                            Email: info@quadb.in
                            </div>
                        </div>
                        <div className={classes.farRight}>
                        <div className={classes.socialLink}>
                                <a href={'https://facebook.com'}><img className={classes.logo} src={fb} alt={'FB'} /></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withStyles(styles)(ExtraHeaderInfo);

