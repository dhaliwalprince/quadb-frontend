import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import Link from './Link';
import Paper from 'material-ui/Paper/Paper';
import Typography from 'material-ui/Typography/Typography';

import fb from './icons/social-facebook-circular-button.svg';
import ig from './icons/instagram-logo.svg';

const styles = theme => ({
    footer: {
        padding: theme.spacing.unit * 4,
        backgroundColor: '#0d4463',
        color: 'white',
    },
    footerPaper: {
        display: 'flex',
        backgroundColor: '#0d4463',
        justifyContent: 'space-around',
        flexWrap: 'wrap',
        maxWidth: '1200px',
        margin: 'auto'
    },
    url: {
        color: 'white',
        textDecoration: 'underline',
    },
    footerSection: {
        flex: 1,
        marginBottom: theme.spacing.unit * 2,
        maxWidth: 200
    },
    links: {
        display: 'flex',
        flexDirection: 'column',
        flexWrap: 'wrap',
    },
    link: {
        marginBottom: theme.spacing.unit,
    },
    sectionTitle: {
        color: 'white',
        letterSpacing: '0.2em',
        textTransform: 'uppercase',
        paddingBottom: theme.spacing.unit * 2,
    },
    span: {
        color: 'white',
    },
    rightLink: {
        textAlign: 'left'
    },
    socialLinks: {
        textAlign: 'center',
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center'
    },
    socialLink: {
        margin: theme.spacing.unit * 2
    },
    logo: {
        height: theme.spacing.unit * 4,
    },
    quadb: {
        marginTop: theme.spacing.unit *2,
        marginLeft: 'auto',
        marginRight: 'auto',
        letterSpacing: '0.3em',
        textAlign: 'center',
        fontSize: '0.8em'
    }
})

const links = [
    {
        name: 'Home',
        to: '/'
    },
    {
        name: 'Cloth Stand',
        to: '/cloth-stand',
    },
    {
        name: 'Designer',
        to: '/designer',
    },
    {
        name: 'Print Varieties',
        to: '/print-varieties'
    },
]

class Footer extends Component {
    render() {
        const { classes } = this.props;

        return (
            <footer className={classes.footer}>
                <Paper className={classes.footerPaper} elevation={0}>
                    <section className={classes.footerSection}>
                        <Typography className={classes.sectionTitle}>
                            <b>Navigate</b>
                        </Typography>
                        <div className={classes.links} style={{ color: 'white' }}>
                            {
                                links.map((link, index) => {
                                    return (
                                        <div className={classes.link}>
                                            <Link className={classes.url} to={link.to}>{link.name}</Link>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </section>
                    <section className={classes.footerSection}>
                        <Typography className={classes.sectionTitle}>
                            <b>Company</b>
                        </Typography>
                        <div className={classes.links}>
                            <div className={classes.link}>
                                <a href={'/about-us'} className={classes.url}>About Us</a>
                            </div>
                            <div className={classes.link}>
                                <a href={'/blog'} className={classes.url}>Blog</a>
                            </div>
                        </div>
                    </section>
                    <section className={classes.footerSection}>
                            <Typography className={classes.sectionTitle}>
                                <b>Contact</b>
                            </Typography>
                            <div className={classes.links}>
                                <div className={classes.link}>
                                    <span className={classes.span}>Weekdays: 9 a.m. - 5 p.m.</span>
                                </div>
                                <div className={classes.rightLink}>
                                    <span className={classes.span}>Marketing Office:<br />
Next 57 cowworking, 2rd floor<br />
Plot 57, Industrial Area Phase 1,<br />Chandigarh 160002 <br />
                                    </span>
                                </div>
                            </div>
                    </section>
                </Paper>
                <div className={classes.social}>
                    <div className={classes.socialLinks}>
                        <div className={classes.socialLink}>
                            <a href={'http://facebook.com'}><img className={classes.logo} src={fb} alt={'FB'} /></a>
                        </div>
                        <div className={classes.socialLink}>
                            <a href={'http://instagram.com'}><img className={classes.logo} src={ig} alt={'IG'} /></a>
                        </div>
                    </div>
                </div>
                <div className={classes.quadb}>
                    <div className={classes.quadbLogo}>
                        <b>QUADB</b> CREATIONS
                    </div>
                </div>
            </footer>
        )
    }
}

export default withStyles(styles)(Footer)
