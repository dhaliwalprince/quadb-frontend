import React from 'react';
import { Typography } from 'material-ui/Typography';
import { withStyles } from 'material-ui';

const styles = theme => ({

})

const FourOFour = ({ classes }) => (
    <div className={classes.root}>
        404 Not found
    </div>
)

export default withStyles(styles)(FourOFour);
