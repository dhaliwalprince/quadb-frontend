import React, { Component } from 'react';
import Typography from 'material-ui/Typography'
import withStyles from 'material-ui/styles/withStyles';
import SwipeableViews from 'react-swipeable-views';

import clothRoll from './carousels/Cloth-roll.jpg';
import customMade from './carousels/Custom-Made-Final.jpg';
import girl from './carousels/girl.jpg';
import ship from './carousels/Ship.jpg';
import Paper from 'material-ui/Paper/Paper';
import Divider from 'material-ui/Divider/Divider';
import Link from 'react-router-dom/Link';

import designerImage from './designer.jpg';

const styles = theme => ({
    slide: {
        height: '50vh',
        display: 'flex',
        width: '100%',
        margin: 'auto'
    },
    background: {
        height: '100%',
        width: '100%'
    },
    headline: {
        margin: 'auto'
    },
    why: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        marginTop: theme.spacing.unit * 2,
        maxWidth: '768px',
        margin: 'auto'
    },
    divider: {
        margin: theme.spacing.unit,
    },
    feature: {
        marginTop: theme.spacing.unit * 2,
        width: 300
    },
    features: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-around'
    },
    image: {
        width: '100%'
    }
})

const slides = [
    {
        title: 'Custom made for your designs',
        options: {},
        image: customMade,
    },
    {
        title: 'Only the best quality fabric',
        options: {},
        image: clothRoll,
    },
    {
        title: 'Don\'t sweat it.',
        options: {},
        image: girl,
    },
    {
        title: 'Guaranteed delivery',
        options: {},
        image: ship,
    }
]

class Slide extends Component {
    render() {
        const { title, image } = this.props.slide;
        const { classes } = this.props;

        return (
            <div className={classes.slide} style={{ background: `url(${image}) center center /cover`}}>
                <div className={classes.headline}>
                    <Typography align='center' type='headline' style={{fontSize: '2em', color: 'white', fontWeight: 'bold'}}>
                        {title.toUpperCase()}
                    </Typography>
                </div>
            </div>
        )
    }
}

const features = [
    {
        icon: '',
        title: 'Most Cloth Variety',
        description: 'From Premium blended cotton to Dry Fit!'
    },
    {
        icon: '',
        title: 'Premium Ink',
        description: "More than just 'printing'!"
    },
    {
        icon: '',
        title: 'Durable Stitching',
        description: "Will always be there for you",
    },
    {
        icon: '',
        title: 'Assured Delivery',
        description: "Don't worry about your package!"
    }
]

class WhyQuadb extends Component {
    render() {
        const { classes } = this.props;
        return (
            <section>
                <Paper className={classes.why} elevation={0}>
                    <div className={classes.whyHeadline}>
                        <Typography type='display1'>
                            We Provide Quality
                        </Typography>
                        <Typography type='caption'>
                            FEATURES
                        </Typography>
                    </div>
                    <div className={classes.features}>
                        {
                            features.map((feature, index) => {
                                return (
                                    <div key={index} className={classes.feature}>
                                        <Typography type='subheading' style={{ fontWeight: 'bold', color: 'black'}}>
                                            {feature.title.toUpperCase()}
                                        </Typography>
                                        <Typography type='subheading'>
                                            { feature.description }
                                        </Typography>
                                    </div>
                                )
                            })
                        }
                    </div>
                </Paper>
            </section>
        )
    }
}

const DesignerLauncher = withStyles(styles)(({ classes }) => {
    return (
        <section className={classes.launcher}>
            <Link to={'/designer'}><img className={classes.image} src={designerImage} /></Link>
        </section>
    )
})

class Home extends Component {
    state = {
        index: 0,
    }

    componentDidMount() {
        this.timer = setInterval(this.handleIndexChange, 2500)
    }

    handleIndexChange = () => {
        this.setState({ index: (this.state.index + 1) % slides.length })
    }

    handleChange = index => {
        this.setState({ index })
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    render() {
        const { classes } = this.props;

        return (
            <div className={classes.root}>
                <DesignerLauncher />
                <WhyQuadb classes={classes} />
            </div>
        )
    }
}

export default withStyles(styles)(Home);
