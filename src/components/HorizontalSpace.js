import React from 'react';
import withStyles from 'material-ui/styles/withStyles';

const styles = theme => ({
    space: {
        height: 1
    }
})
const HorizontalSpace = ({ width, classes }) => (
    <div className={ classes.space } />
)

export default withStyles(styles)(HorizontalSpace);
