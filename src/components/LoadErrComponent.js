import React, { Component, Fragment } from 'react';
import Error from '../components/Error'
import Loader from '../components/Loader'
import withStyles from 'material-ui/styles/withStyles';

const styles = theme => ({

})

class LoadErrComponent extends Component {
    render() {
        const { classes, loading, error, component } = this.props;

        if (loading) {
            return <Loader classes={classes} />
        }

        return (
            <Fragment>
                { error && <Error error={error} />}
                { !error && (component || this.props.children) }
            </Fragment>
        )
    }
}

export default withStyles(styles)(LoadErrComponent);
