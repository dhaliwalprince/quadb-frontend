import React, { Component } from "react";
import { CircularProgress } from 'material-ui/Progress';

const Loader = ({ classes }) => (
    <div className={classes.progress}>
        <CircularProgress />
    </div>
)

export default Loader;
