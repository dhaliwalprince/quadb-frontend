import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import Typography from 'material-ui/Typography';
import { Link } from 'react-router-dom'
import Button from 'material-ui/Button/Button';

const styles = theme => ({
    paper: {
            padding: theme.spacing.unit,
            textAlign: 'center',
            width: '100%'
        }
})

class LoginOrSignupOption extends Component {
    render() {
        const { classes } = this.props;

        if (this.props.short) {
            return (
                <div className={classes.short}>
                    <Button component={Link} to={'/login'}>
                        Login / Signup
                    </Button>
                </div>
            )
        }
        return (
            <div className={ classes.paper}>
                <Typography align='center'>
                    Please <Link to="/login">login</Link> or <Link to="/signup">sign up</Link> to view this page.
                </Typography>                
            </div>
        )
    }
}

export default withStyles(styles)(LoginOrSignupOption)
