import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import { Link } from 'react-router-dom'
import Typography from 'material-ui/Typography'
import Paper from 'material-ui/Paper'
import UserApi from '../globals/user'
import { LinearProgress } from 'material-ui/Progress'
import Form from '../containers/Form'
import withRouter from 'react-router-dom/withRouter';

const styles = theme => ({
    wrapper: {
        display: 'block',
        margin: 'auto',
        maxWidth: 480
    },
    form: {
        padding: theme.spacing.unit * 2
    },

    formControl: {
    },
    errorMessage: {
        marginBottom: theme.spacing.unit
    },
    footer: {
        marginTop: theme.spacing.unit * 2
    }
})

class LoginPage extends Component {
    state = {
        errors: null
    }
    emailInput = null
    passwordInput = null

    continueNext() {
        window.location = '/'
    }

    handleSubmit = data => {
        this.setState({ errors: null, submitting: true })
        const api = UserApi.GetInstance()

        api.login(data.email, data.password, {
            onSuccess: () => {
                // continue to the page that was requested
                this.continueNext()
            },
            onFailed: (err) => {
                this.setState(() => ({ submitting: false, errors: { email: ' ', password: ' ' }, globalError: err}))
            }
        })
    }

    render() {
        const { classes } = this.props;

        const columns = [
            {
                type: 'email',
                name: 'Email',
                field: 'email',
                required: true,
                errorMessage: 'Invalid email.',
            },
            {
                type: 'password',
                name: 'Password',
                required: true,
                field: 'password',
                errorMessage: 'Invalid password.'
            }
        ]

        return (
            <div className={classes.wrapper}>
                {this.state.submitting && <LinearProgress />}
                <div classes={ { root: classes.form } }>
                    <Form
                        onSubmit={this.handleSubmit}
                        columns={columns}
                        globalError={this.state.globalError}
                        disabled={this.state.submitting}
                        submitLabel={'Login'}
                    />

                    <div className={classes.footer}>
                        <Typography><Link to="/signup">Create an account.</Link></Typography>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(withStyles(styles)(LoginPage))
