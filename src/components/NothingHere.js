import React from 'react';
import Typography from 'material-ui/Typography/Typography';


export default function NothingHere() {
    return (
        <Typography type='caption'>
            Nothing here right now. Check back later.
        </Typography>
    )
}