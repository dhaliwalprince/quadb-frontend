import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import OrderEventDetails from './OrderEventDetails';
import Typography from 'material-ui/Typography'
import OrderDetails from './OrderDetails';
import Paper from 'material-ui/Paper';
import Orders from '../globals/order';
import { DialogActions } from 'material-ui/Dialog';
import Button from 'material-ui/Button';

const styles = theme => ({
    root: {
        maxWidth: 480,
        display: 'block',
        margin: 'auto',
        marginBottom: theme.spacing.unit * 2
    },
    paper: {
        padding: theme.spacing.unit
    },
    details: {
        padding: theme.spacing.unit
    }
})

class Order extends Component {
    render() {
        const { classes, order, onEdit, onCancel } = this.props;

        return (
            <div className={classes.root}>
                <Paper className={classes.paper}>
                    <OrderEventDetails events={order.events} order={order} />
                    <div className={classes.details}>
                        <Typography>
                            Order #: <b>{order.id}</b>
                        </Typography>
                        <Typography type={'caption'}>
                            Order details:
                        </Typography>
                        <OrderDetails details={order.order_details} order={order} />
                    </div>
                    <DialogActions>
                        <Button disabled={Orders.utils.isAccepted(order)} onClick={onEdit}>
                            Edit
                        </Button>
                        <Button disabled={Orders.utils.isAccepted(order)} color={'error'} onClick={onCancel}>
                            Cancel
                        </Button>
                    </DialogActions>
                </Paper>
            </div>
        )
    }
}

export default withStyles(styles)(Order)
