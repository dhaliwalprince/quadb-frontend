import React from 'react';
import { withStyles } from "material-ui/styles";
import Typography from "material-ui/Typography";
import Divider from 'material-ui/Divider';

const styles = theme => ({
    divider: {
        marginTop: theme.spacing.unit * 2,
        marginBottom: theme.spacing.unit * 2
    },
    designThumbnail: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    }
})

const OrderDetails = ({ classes, details }) => {
    return (
        <div className={classes.root}>
            <Divider className={classes.divider} />
            <div className={classes.extraInfo}>
                <Typography>
                    Product: {details.category.name}({details.cloth_material.name})
                </Typography>
            </div>
            <Divider className={classes.divider} />
            <div className={classes.designThumbnail}>
                <img src={details.design.image_url} alt={details.design.name} />
                <Typography type={'caption'}>
                    {details.design.name}
                </Typography>
            </div>
            <Divider className={classes.divider} />
            
            <div className={classes.pricing}>
                <Typography>
                    Quantity: <b>{details.quantity}</b>
                </Typography>
                <Typography>
                    Pricing: <b>{details.quantity}</b> * <b>{details.item_price}</b> = <b>{details.item_price * details.quantity}</b>
                </Typography>
            </div>
        </div>
    )
}

export default withStyles(styles)(OrderDetails)
