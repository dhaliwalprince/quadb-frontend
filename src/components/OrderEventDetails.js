import React, { Component } from 'react';
import withStyles from 'material-ui/styles/withStyles';
import Stepper from 'material-ui/Stepper/Stepper';
import Step from 'material-ui/Stepper/Step';
import StepLabel from 'material-ui/Stepper/StepLabel';
import Orders from '../globals/order';

const styles = theme => ({
    paper: {
        padding: theme.spacing.unit
    }
})

const eventTypes = [
    'ORDER_PLACED',
    'ORDER_ACCEPTED',
    'ORDER_DELIVERED',
]

const eventMapping = {
    ORDER_PLACED: '30:created',
    ORDER_ACCEPTED: '40:accepted',
    ORDER_DELIVERED: '100:delivered',
    ORDER_CANCELLED: '200:cancelled',
    ORDER_REJECTED: '500:rejected'
}

const events = [
    {
        name: 'Placed',
        type: eventMapping.ORDER_PLACED,
    },
    {
        name: 'Accepted',
        type: eventMapping.ORDER_ACCEPTED,
    },
    {
        name: 'Delivered',
        type: eventMapping.ORDER_DELIVERED,
    }
];

const OrderEventDetails = ({ classes, order }) => {

    let activeStep = 0;
    if (Orders.utils.isDelivered(order)) {
        activeStep = events.findIndex(event => event.type === eventMapping.ORDER_DELIVERED);
    } else if (Orders.utils.isAccepted(order)) {
        activeStep = events.findIndex(event => event.type === eventMapping.ORDER_ACCEPTED);
    } else if (Orders.utils.isPlaced(order)) {
        activeStep = events.findIndex(event => event.type === eventMapping.ORDER_PLACED);
    }

    return (
        <div className={classes.root}>
            <Stepper classes={{ root: classes.paper }} activeStep={activeStep}>
                {
                    events.map((event, index) => {
                        return (
                            <Step key={index}>
                                <StepLabel>{event.name}</StepLabel>
                            </Step>
                        )
                    })
                }
            </Stepper>
            
        </div>
    )
}

export default withStyles(styles)(OrderEventDetails)
