import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import Order from './Order'
import NothingHere from './NothingHere';

const styles = theme => ({
    root: {
        maxWidth: 480,
        display: 'block',
        margin: 'auto',
    },
    paper: {
        padding: theme.spacing.unit,
        marginBottom: theme.spacing.unit * 2
    },
    details: {
        padding: theme.spacing.unit
    }
})

class OrderList extends Component {
    render() {
        const { classes, orders, onEdit, onCancel } = this.props;

        return (
            <div className={classes.root}>
                {
                    orders.map((order, index) => {
                        return (
                            <Order order={order} key={index} onEdit={() => onEdit(order)} onCancel={() => onCancel(order)} />
                        )
                    })
                }
                {
                    orders.length === 0 && <NothingHere />
                }
            </div>
        )
    }
}

export default withStyles(styles)(OrderList)