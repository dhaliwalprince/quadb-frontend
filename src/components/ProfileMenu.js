import React, { Component, Fragment } from 'react';
import Menu, { MenuItem }  from 'material-ui/Menu';
import {ListItemIcon, ListItemText} from 'material-ui/List'
import { withRouter } from 'react-router-dom';
import { AuthorizedApi } from '../globals/authorization';
import Link from './Link';

const urls = [
    {
        id: 1,
        title: 'Your Profile',
        link: '/profile',
        icon: 'account_circle'
    },
    {
        id: 2,
        title: 'Manage addresses',
        link: '/manage/addresses',
        icon: 'book'
    },
    {
        id: 3,
        title: 'Your requests',
        link: '/manage/requests',
        icon: 'shopping_basket',
    },
    {
        id: 4,
        title: 'Your orders',
        link: '/manage/orders',
        icon: 'shopping_basket',
    },
    {
        id: 6,
        title: 'Your designs',
        link: '/manage/designs',
        icon: 'color_lens',
    },
    {
        id: 7,
        title: 'Logout',
        link: '/logout',
        icon: 'exit_to_app'
    }
]

function LogInOrSignUpOption() {
    return (
        <Fragment>
            <MenuItem component={Link} to='/login'>
                <ListItemIcon>
                    <i className={"material-icons"}>supervisor_account</i>
                </ListItemIcon>
                <ListItemText primary={'Login'} />
            </MenuItem>
            <MenuItem component={Link} to='/register'>
                <ListItemIcon>
                    <i className={"material-icons"}>supervisor_account</i>
                </ListItemIcon>
                <ListItemText primary={'Sign Up'} />
            </MenuItem>
        </Fragment>
    )
}

class ProfileMenu extends Component {
    static ID = 'profile-menu'

    toUrl = (url, event) => {
        this.props.history.push(url)
        this.props.onRequestClose(event)
    }

    render() {
        const { anchorEl, open, onRequestClose } = this.props;

        return (
            <Menu
                id={ProfileMenu.ID}
                anchorEl={anchorEl}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                open={open}
                onRequestClose={onRequestClose}
            >
            {
                AuthorizedApi.authApi.loggedIn ? 
                    urls.map((url, i) => (
                        <MenuItem key={i} onClick={this.toUrl.bind(this, url.link)}>
                            <ListItemIcon>
                                <i className={"material-icons"}>{url.icon}</i>
                            </ListItemIcon>
                            <ListItemText primary={url.title} secondary={url.description} />
                        </MenuItem>
                    )) : <LogInOrSignUpOption />
            }
            </Menu>
        )
    }
}

export default withRouter(ProfileMenu);
