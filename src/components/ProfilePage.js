import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import Typography from 'material-ui/Typography';
import Paper from 'material-ui/Paper'
import { Link } from 'react-router-dom'

const styles = theme => ({
    root: {
        maxWidth: 480,
        padding: theme.spacing.unit * 2,
        display: 'block',
        margin: 'auto'
    },
    profileSection: {
        marginTop: theme.spacing.unit,
        marginBottom: theme.spacing.unit
    }
})

const ProfilePage = ({ user, classes }) => (
    <Paper className={classes.root}>
        <div className={classes.profileSection}>
            <Typography type={'caption'}>
                Name
            </Typography>
            <Typography>
                {user.name}
            </Typography>
        </div>
        <div className={classes.profileSection}>
            <Typography type={'caption'}>
                Email
            </Typography>
            <Typography>
                {user.email}
            </Typography>
        </div>
        <div className={classes.profileSection}>
            <Typography>
                <Link to="/logout">Logout</Link>
            </Typography>
        </div>
    </Paper>
)

export default withStyles(styles)(ProfilePage);
