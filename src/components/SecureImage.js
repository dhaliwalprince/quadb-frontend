import React, { Component } from 'react';
import LoadErrComponent from './LoadErrComponent';
import { AuthorizedApi } from '../globals/authorization';

class SecureImage extends Component {
    static count = 0

    state = {
        loading: true,
    }

    componentDidMount() {
        const headers = new Headers()
        headers.set('Authorization', AuthorizedApi.authApi.getToken())
        fetch('/api/assets/images/private/' + this.props.image_url, { headers })
            .then(res => res.blob() || res.json())
            .then(blob => {
                const url = URL.createObjectURL(blob)
                this.setState({ image: url, loading: false })
            })
            .catch(err => {
                this.setState({ error: err, loading: false })
            })
    }
    
    handleRetry = () => {
        this.setState({ loading: true, error: false })
        this.componentDidMount()
    }

    render() {
        return (
            <LoadErrComponent loading={this.state.loading} error={this.state.error} onRetry={this.handleRetry}>
                { this.state.loading || 
                    <img src={this.state.image} {...this.props} />
                }
            </LoadErrComponent>
        )
    }
}

export default SecureImage;
