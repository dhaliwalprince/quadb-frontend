import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import { Link, withRouter } from 'react-router-dom'
import Typography from 'material-ui/Typography'
import Paper from 'material-ui/Paper'
import UserApi from '../globals/user'
import { LinearProgress } from 'material-ui/Progress'
import Form from '../containers/Form'

const styles = theme => ({
    wrapper: {
        display: 'block',
        margin: 'auto',
        maxWidth: 480
    },
    form: {
        display: 'flex',
        flexDirection: 'column',
        margin: 'auto',
        padding: theme.spacing.unit * 2,
    },
    formControl: {
    },
    errorMessage: {
        marginBottom: theme.spacing.unit
    },

    footer: {
        marginTop: theme.spacing.unit * 2
    }
})

class SignUpPage extends Component {
    state = {
        errors: null
    }

    nameInput = null
    emailInput = null
    passwordInput = null
    rpasswordInput = null

    continueNext() {
        const { history } = this.props
        history.push('/login')
    }

    handleSubmit = (data) => {

        this.setState({ errors: null, submitting: true })
        const api = UserApi.GetInstance()

        api.signup(data.name, data.email, data.password, {
            onSuccess: () => {
                // continue to the page that was requested
                this.continueNext()
            },
            onFailed: (err) => {
                if (err.errors)
                    err.message = err.errors.join('<br />') + '<br />'
                this.setState(() => ({ submitting: false, globalError: err}))
            }
        })
    }

    render() {
        const { classes } = this.props;

        const columns = [
            {
                type: 'name',
                name: 'Name',
                field: 'name',
                required: true,
                errorMessage: 'Invalid name',
                validation: /[a-bA-B ]{,128}/
            },
            {
                type: 'email',
                name: 'Email',
                field: 'email',
                required: true,
                errorMessage: 'Invalid email'
            },
            {
                type: 'password',
                name: 'Password',
                field: 'password',
                required: true,
                errorMessage: 'Invalid password',
                validation: /[a-bA-B0-9@#$!]{6,128}/
            }
        ]

        return (
            <div className={classes.wrapper}>
                {this.state.submitting && <LinearProgress />}
                <div classes={ { root: classes.form } }>
                    <Form
                        onSubmit={this.handleSubmit}
                        columns={columns}
                        globalError={this.state.globalError}
                        disabled={this.state.submitting}
                        submitLabel={'Sign up'}
                    />
                    <div className={classes.footer}>
                        <Link to="/login"><Typography>Already registered? Log in.</Typography></Link>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(withStyles(styles)(SignUpPage))