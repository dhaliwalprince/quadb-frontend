import React, { Component } from 'react';
import Paper from 'material-ui/Paper/Paper';
import ConfigurationView from './ConfigurationView';
import withStyles from 'material-ui/styles/withStyles';
import Button from 'material-ui/Button/Button';
import Link from 'react-router-dom/Link';
import Grid from 'material-ui/Grid/Grid';
import Typography from 'material-ui/Typography/Typography';
import Divider from 'material-ui/Divider/Divider';

const styles = theme => ({
    paper: {
        padding: theme.spacing.unit,
    },
    form: {
        marginTop: theme.spacing.unit,
    },
    divider: {
        marginTop: theme.spacing.unit,
    },
    row: {
        display: 'flex',
        flexDirection: 'row',
    },
    heading: {
        fontWeight: 'bold',
    }
})

class CSelectedConfiguration extends Component {
    render() {
        const { classes, columns, selected } = this.props;

        return (
            <div className={classes.root}>
                {
                    columns.map((column, id) => {
                        return (
                            <div key={id} className={classes.row}>
                                <Typography className={classes.heading}>
                                    {column.name}:&nbsp;
                                </Typography>
                                <Typography className={classes.value}>
                                    { selected[column.key]}
                                </Typography>
                            </div>
                        )
                    })
                }
            </div>
        )
    }
}

const SelectedConfiguration = withStyles(styles)(CSelectedConfiguration);
export { SelectedConfiguration };

/**
 * actions is an object of type
 * {
 *  'select': {
 *      name: 'Select',
 *      callback: () => {}
 *  },
 *  'create': {
 *      name: 'Create',
 *      type: 'link',
 *      url: '/some-url'
 *  }
 * }
 */
class Configuration extends Component {
    state = {
        hasSelection: false,
        selected: null,

    }

    handleSelect = row => {
        this.setState({ selected: row, hasSelection: true });
        this.props.onSelect(row);
    }

    renderActions() {
        const { actions } = this.props;
        const actionButtons = [];

        const defaultCallback = () => {};
        let i = 0;
        if ('create' in actions) {
            const prop = 'create';
            if (actions[prop].type === 'link') {
                actionButtons.push(<Button raised key={i++} component={Link} to={actions[prop].url}>{actions[prop].name}</Button>)
            } else if (actions[prop].type === 'button') {
                actionButtons.push(<Button raised key={i++} onClick={actions[prop].callback || defaultCallback}>{actions[prop].name}</Button>)
            }
        }

        return actionButtons;
    }

    renderSelectionActions() {
        const { actions } = this.props;
        const actionButtons = [];

        const defaultCallback = () => {};
        let i = 0;
        for (let prop in actions) {
            if (prop === 'select' || prop === 'create') {
                continue;
            }

            console.log(actions[prop]);
            if (actions[prop].type === 'link') {
                actionButtons.push(<Button raised key={i++} component={Link} to={actions[prop].url}>{actions[prop].name}</Button>)
            } else if (actions[prop].type === 'button') {
                actionButtons.push(<Button raised key={i++} onClick={actions[prop].callback || defaultCallback}>{actions[prop].name}</Button>)
            }
        }

        if (actionButtons.length === 0) {
            return <Typography type='caption'>No action available</Typography>
        }

        console.log(actionButtons);
        return <div>{actionButtons}</div>;
    }
    
    render() {
        const { classes, columns, configurations } = this.props

        return (
            <div className={classes.root}>
                <Paper className={classes.paper}>
                    <ConfigurationView onSelect={this.handleSelect} columns={columns} data={configurations} />
                    <div>
                        { this.renderActions() }
                    </div>
                    <Divider className={classes.divider}/>

                    <div className={classes.form}>
                        <Typography type='caption'>
                        Selected
                        </Typography>
                        {
                            this.state.hasSelection ? <SelectedConfiguration selected={this.state.selected} columns={columns} />
                                    : <Typography type='caption'>Nothing selected</Typography>
                        }
                        { this.state.hasSelection &&
                            <div className={classes.selectionActions}>
                                <Typography type='caption'>
                                Selected actions
                                </Typography>
                                {
                                    this.renderSelectionActions()
                                }
                            </div>
                        }
                    </div>
                </Paper>
            </div>
        )
    }
}

export default withStyles(styles)(Configuration);
