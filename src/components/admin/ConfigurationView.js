import React, { Component } from 'react';
import ReactDataGrid from 'react-data-grid';
import withStyles from 'material-ui/styles/withStyles';

const styles = theme => ({

})

class ConfigurationView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            rows: props.data,
        }
    }

    rowGetter = (i) => {
        return this.state.rows[i];
    }

    handleRowClick = (row) => {
        this.props.onSelect(this.state.rows[row])
    }

    render() {
        const { columns, classes } = this.props;
        return (
            <div className={classes.root}>
                <ReactDataGrid
                    columns={columns}
                    rowGetter={this.rowGetter}
                    onRowClick={this.handleRowClick}
                    rowsCount={this.state.rows.length}
                    height={window.innerHeight - 72}
                />
            </div>
        )
    }
}

export default withStyles(styles)(ConfigurationView);