import React, { Component } from 'react';
import withStyles from 'material-ui/styles/withStyles';
import OrderDetails from '../OrderDetails';
import Typography from 'material-ui/Typography/Typography';
import DialogActions from 'material-ui/Dialog/DialogActions';
import Orders from '../../globals/order';
import Button from 'material-ui/Button/Button';
import Paper from 'material-ui/Paper/Paper';

const styles = theme => ({
    root: {
        padding: theme.spacing.unit * 2,
    }
})

class Order extends Component {
    render() {
        const { classes, order, onAccept, onReject, onDelivered } = this.props;
        return (
            <Paper className={classes.root}>
                <div className={classes.details}>
                    <Typography>
                        Order #: <b>{order.id}</b>
                    </Typography>
                    <Typography type={'caption'}>
                        Order details:
                    </Typography>
                    <OrderDetails details={order.order_details} order={order} />
                </div>
                <DialogActions>
                    <Button disabled={Orders.utils.isAccepted(order)} onClick={() => onAccept(order)}>
                        Accept
                    </Button>
                    <Button disabled={Orders.utils.isAccepted(order)} onClick={() => onReject(order)}>
                        Reject
                    </Button>
                    <Button disabled={!Orders.utils.isAccepted(order)} onClick={() => onDelivered(order)}>
                        Delivered
                    </Button>
                </DialogActions>
            </Paper>
        )
    }
}

export default withStyles(styles)(Order);
