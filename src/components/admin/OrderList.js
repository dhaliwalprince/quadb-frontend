import React from 'react';
import Order from './Order';
import withStyles from 'material-ui/styles/withStyles';

const styles = theme => ({
    root: {
        maxWidth: '800px',
        margin: 'auto',
    }
})

const OrderList = ({ orders, classes, ...other }) => (
    <div className={classes.root}>
        {
            orders.map((order, index) => (
                <Order order={order} key={index} {...other}/>
            ))
        }
    </div>
)

export default withStyles(styles)(OrderList);
