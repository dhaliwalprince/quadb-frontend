import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import List, { ListItem, ListItemText } from 'material-ui/List';
import Dialog, { DialogActions, DialogContent, DialogTitle } from 'material-ui/Dialog';
import Loader from '../components/Loader';
import UserApi from '../globals/user';
import Error from '../components/Error';
import Typography from 'material-ui/Typography/Typography';
import Link from 'react-router-dom/Link';

const styles = theme => ({

})

class AddressChooser extends Component {
  state = {
      loading: true,
      error: false,
  }
  handleChange = address => {
      this.props.onChange(address);
  }

  componentDidMount() {
      const user = UserApi.GetInstance()
      user.addresses.all({
          onSuccess: addresses => {
              this.setState(() => ({ addresses, loading: false, error: false }))
          },
          onFailed: error => {
              this.setState(() => ({ error }))
          }
      })
  }

  render() {
    const { classes, ...other } = this.props;

    return (
      <Dialog
        {...other}
      >
        <DialogTitle>
            Select an address.
        </DialogTitle>
      {
        <DialogContent>
                  {
                    this.state.error || this.state.loading ? (
                        this.state.error ? <Error error={this.state.error} /> : <Loader classes={classes} />
                    ) : (
                    <List>
                    {
                        this.state.addresses.map(address => (
                            <ListItem key={address.id} button onClick={this.handleChange.bind(this, address)}>
                                <ListItemText primary={address.toOneLine()} />
                            </ListItem>
                        ))
                    }
                    {
                        this.state.addresses.length === 0 && 
                            <Typography>
                                No addresses in your address book. Add addresses <Link to='/manage/addresses'>here.</Link>
                            </Typography>
                    }
                    </List>
                )}
        </DialogContent>
      }
      </Dialog>
    );
  }
}

export default withStyles(styles)(AddressChooser);
