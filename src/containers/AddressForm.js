import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import { Link } from 'react-router-dom'
import Input, { InputLabel } from 'material-ui/Input';
import { FormControl, FormHelperText } from 'material-ui/Form';
import { DialogControls } from 'material-ui/Dialog'
import Typography from 'material-ui/Typography'
import Button from 'material-ui/Button'
import Paper from 'material-ui/Paper'
import UserApi from '../globals/user'
import { LinearProgress } from 'material-ui/Progress'

import NavigateNext from 'material-ui-icons/NavigateNext'

const styles = theme => ({
    wrapper: {
        display: 'block',
        margin: 'auto',
        width: '100%'
    },
    form: {
        display: 'flex',
        flexDirection: 'column',
        margin: 'auto',
        padding: theme.spacing.unit * 2,
    },
    formControlButton: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'right',
    },
    errorMessage: {
        marginBottom: theme.spacing.unit
    },
    footer: {
        marginTop: theme.spacing.unit * 2
    }
})

class AddressForm extends Component {
    state = {
        errors: null
    }
    labelInput = null
    line1Input = null
    line2Input = null
    cityInput = null
    stateInput = null
    pincodeInput = null
    mobileInput = null

    continueNext() {
        const { history } = this.props
        history.push('/')
    }

    handleSubmit = (event) => {
        event.preventDefault()
        event.stopPropagation()

        const label = this.labelInput.value;
        const line1 = this.line1Input.value;
        const line2 = this.line2Input.value;
        const city = this.cityInput.value;
        const state = this.stateInput.value;
        const pincode = this.pincodeInput.value;
        const mobile = this.mobileInput.value;

        const errors = {}
        if (line1.length < 1) {
            errors.line1 = 'Line 1 cannot be empty'
        }

        if (city.length < 1) {
            errors.city = 'City cannot be empty'
        }

        if (state.length < 1) {
            errors.state = 'State cannot be empty'
        }

        if (pincode.length !== 6) {
            errors.pincode = 'Invalid pincode'
        }

        if (mobile.length < 10 && mobile.length > 12) {
            errors.mobile = 'Invalid mobile'
        }

        if (errors.line1 || errors.city || errors.state || errors.pincode || errors.mobile) {
            this.setState(() => ({ errors }))
            return
        }
        
        this.setState({ errors: null, submitting: true })
        const api = UserApi.GetInstance()

        api.addresses.save({ label, line1, line2, city, state, pincode, mobile }, {
            onSuccess: () => {
                this.setState(() => ({
                    submitting: false,
                    success: true,
                    errors: null,
                    globalError: null
                }))

                this.props.onRequestClose({shouldUpdate: true})
            },
            onFailed: (err) => {
                this.setState(() => ({ submitting: false, errors: { }, globalError: err}))
            }
        })
    }

    render() {
        const { classes } = this.props;

        const { errors } = this.state;

        return (
            <div className={classes.wrapper}>
                {this.state.submitting && <LinearProgress />}
                <Paper component={'form'} onSubmit={this.handleSubmit} autoComplete="off" classes={ { root: classes.form } }>
                    <Typography color={'error'} className={classes.errorMessage}>{this.state.globalError && this.state.globalError.message}</Typography>

                    <FormControl disabled={this.state.submitting} className={classes.formControl} error={errors && typeof errors.label !== 'undefined'}>
                        <InputLabel htmlFor="label">Label</InputLabel>
                        <Input type="text" id="label" name="label" autoComplete="off" inputRef={ref => this.labelInput = ref} />
                        <FormHelperText>{errors && typeof errors.label !== 'undefined' && errors.label}</FormHelperText>
                    </FormControl>
                    
                    <FormControl disabled={this.state.submitting} className={classes.formControl} error={errors && typeof errors.line1 !== 'undefined'}>
                        <InputLabel htmlFor="line1">Line 1</InputLabel>
                        <Input type="line1" id="line1" name="line1" autoComplete="off" inputRef={ref => this.line1Input = ref} />
                        <FormHelperText>{errors && typeof errors.line1 !== 'undefined' && errors.line1}</FormHelperText>
                    </FormControl>
                    
                    <FormControl disabled={this.state.submitting} className={classes.formControl} error={errors && typeof errors.line2 !== 'undefined'}>
                        <InputLabel htmlFor="line2">Line 2</InputLabel>
                        <Input type="line2" id="line2" name="line2" autoComplete="off" inputRef={ref => this.line2Input = ref} />
                        <FormHelperText>{errors && typeof errors.pincode !== 'undefined' && errors.pincode}</FormHelperText>
                    </FormControl>
                    
                    <FormControl disabled={this.state.submitting} className={classes.formControl} error={errors && typeof errors.city !== 'undefined'}>
                        <InputLabel htmlFor="city">City</InputLabel>
                        <Input type="city" id="city" name="city" autoComplete="off" inputRef={ref => this.cityInput = ref} />
                        <FormHelperText>{errors && typeof errors.city !== 'undefined' && errors.city}</FormHelperText>
                    </FormControl>
                    
                    <FormControl disabled={this.state.submitting} className={classes.formControl} error={errors && typeof errors.state !== 'undefined'}>
                        <InputLabel htmlFor="state">State</InputLabel>
                        <Input type="state" id="state" name="state" inputRef={ref => this.stateInput = ref} />
                        <FormHelperText>{errors && typeof errors.state !== 'undefined' && errors.state}</FormHelperText>
                    </FormControl>

                    <FormControl disabled={this.state.submitting} className={classes.formControl} error={errors && typeof errors.pincode !== 'undefined'}>
                        <InputLabel htmlFor="pincode">Pincode</InputLabel>
                        <Input type="pincode" id="pincode" name="pincode" inputRef={ref => this.pincodeInput = ref} />
                        <FormHelperText>{errors && typeof errors.pincode !== 'undefined' && errors.pincode}</FormHelperText>
                    </FormControl>

                    <FormControl disabled={this.state.submitting} className={classes.formControl} error={errors && typeof errors.mobile !== 'undefined'}>
                        <InputLabel htmlFor="mobile">Mobile</InputLabel>
                        <Input type="mobile" id="mobile" name="Mobile" inputRef={ref => this.mobileInput = ref} />
                        <FormHelperText>{errors && typeof errors.mobile !== 'undefined' && errors.mobile}</FormHelperText>
                    </FormControl>

                    <FormControl className={classes.formControlButton}>
                        <Button disabled={this.state.submitting} type="submit" className={classes.submitButton}>
                            Save
                            <NavigateNext className={classes.rightIcon} />
                        </Button>
                        <Button className={classes.submitButton} onClick={this.props.onRequestClose}>
                            Cancel
                        </Button>
                    </FormControl>
                </Paper>
            </div>
        )
    }
}

export default withStyles(styles)(AddressForm)
