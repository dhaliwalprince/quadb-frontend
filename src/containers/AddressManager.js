import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import auth from './auth'
import AddressList from '../components/AddressList';
import Button from 'material-ui/Button';
import { CircularProgress } from 'material-ui/Progress';
import UserApi from '../globals/user';
import AddNewAddressDialog from '../components/AddNewAddressDialog'
import Error from '../components/Error';
import Typography from 'material-ui/Typography'
import Dialog, { DialogTitle, DialogContentText, DialogContent, DialogActions } from 'material-ui/Dialog'

const styles = theme => ({
    root: {
        maxWidth: '480px',
        display: 'block',
        margin: 'auto'
    },
    title: {
        marginLeft: theme.spacing.unit * 2
    },
    progressWrapper: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around'
    }
});

class ConfirmDialog extends Component {
    render() {
        const { title, extraMessage, onConfirm, onCancel, ...other} = this.props;
        return (
            <Dialog {...other}>
                <DialogTitle>{title}</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        {extraMessage && extraMessage}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={onCancel} autoFocus>
                        Cancel
                    </Button>
                    <Button onClick={onConfirm} color="primary">
                        Confirm
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }
}

class AddressManager extends Component {
    state = {
        error: null,
        loading: false,
        addresses: [],
        openDialog: false,
        confirmDialog: false,
        errorDialog: true,
    }

    handleAddNewAddress = () => {
        this.setState(() => ({ openDialog: true }))
    }

    loadAddresses() {
        const api = UserApi.GetInstance();
        api.addresses.all({
            onSuccess: addresses => {
                this.setState(() => ({ loading: false, addresses, error: null }))
            },
            onFailed: error => {
                this.setState(() => ({ loading: false, error }))
            }
        })
    }

    componentDidMount() {
        this.loadAddresses()        
    }

    refresh() {
        this.setState(() => ({ loading: true, error: null, confirmDialog: false, openDialog: false }))
        this.loadAddresses()
    }

    handleRequestClose = params => {
        if (params && params.shouldUpdate) {
            this.refresh()
        } else {
            this.setState(() => ({ openDialog: false }))
        }
    }

    deleteAddress = () => {
        const api = UserApi.GetInstance();
        api.addresses.delete(this.state.addressToDelete, {
            onSuccess: () => {
                this.refresh()
            },
            onFailed: error => {
                this.setState({ confirmDialog: false, errorDialog: true, actionError: {message: 'Unable to delete address.'} })
            }
        })
    }

    handleDeleteAction = address => {
        this.setState({ confirmDialog: true, addressToDelete: address })
    }

    handleConfirmDialogClose = () => {
        this.setState({ confirmDialog: false })
    }

    handleErrorDialogClose = () => {
        this.setState({ errorDialog: false })
    }

    render() {
        const { classes } = this.props;
        if (this.state.loading) {
            return (
                <div className={classes.progressWrapper}>
                    <CircularProgress />
                </div>
            )
        }

        if (this.state.error) {
            return (
                <div className={classes.progressWrapper}>
                    <Error error={this.state.error} />
                </div>
            )
        }

        return (
            <div className={classes.root}>
                <Typography type={'title'} className={classes.title}>Your addresses</Typography>

                <AddressList addresses={this.state.addresses} onDeleteAction={this.handleDeleteAction} />
                <div className={classes.addButton}>
                    <Button onClick={this.handleAddNewAddress}>
                        Add new address
                    </Button>
                    <AddNewAddressDialog maxWidth={'md'} onRequestClose={this.handleRequestClose} open={this.state.openDialog} />
                </div>
                
                <ConfirmDialog
                    title="Are you sure to delete the address?"
                    onConfirm={this.deleteAddress}
                    onCancel={this.handleConfirmDialogClose}
                    open={this.state.confirmDialog}
                    />
            </div>
        )
    }
}

export default withStyles(styles)(auth(AddressManager));
