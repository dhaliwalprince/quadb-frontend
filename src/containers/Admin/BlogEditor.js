import React, { Component } from 'react';
import { Editor, EditorState } from 'draft-js';
import { withStyles } from 'material-ui';

import 'draft-js/dist/Draft.css';

const styles = theme => ({

})

class BlogEditor extends Component {
    constructor(props) {
        super(props);

        this.state = {
            editorState: EditorState.createEmpty(),
        }
    }

    handleChange = editorState => {
        this.setState({ editorState })
    }

    render() {
        const { classes } = this.props;

        return (
            <div className={classes.root}>
                <Editor editorState={this.state.editorState} onChange={this.handleChange} />
            </div>
        )
    }
}

export default withStyles(styles)(BlogEditor);
