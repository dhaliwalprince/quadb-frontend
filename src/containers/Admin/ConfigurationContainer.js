import React, { Component } from 'react';
import withStyles from 'material-ui/styles/withStyles';
import { AuthorizedApi } from '../../globals/authorization';
import LoadErrComponent from '../../components/LoadErrComponent';
import Configuration from '../../components/admin/Configuration';
import CreateConfigurationDialog from './CreateConfigurationDialog';
import DeleteConfigurationDialog from './DeleteConfigurationDialog';
import Typography from 'material-ui/Typography/Typography';
import UpdateConfigurationDialog from './UpdateConfigurationDialog';
import LinearProgress from 'material-ui/Progress/LinearProgress';

const styles = theme => ({

})

export class InMemoryStore {
    store = {}

    get(key) {
        return this.store[key]
    }

    set(key, value) {
        this.store[key] = value;
    }
}

export class PropertyCheckProxy {
    constructor(store) {
        this.store = store;
        this._initHandlers();
    }

    get(key) {
        return this.proxy.get(key)
    }

    set(key, value) {
        return this.proxy.set(key, value);
    }

    _initHandlers() {
        this.proxy = new Proxy(this.store, {
            get: function(target, name) {
                if (name in target) {
                    return target[name];
                } else {
                    throw new TypeError(`Property ${name} is required.`)
                }
            }
        });
    }
}

export class Map {
    constructor(store) {
        if (typeof store === 'undefined') {
            this.store = new PropertyCheckProxy(new InMemoryStore());
        } else {
            this.store = new PropertyCheckProxy(store);
        }
    }

    set(key, value) {
        return this.store.set(key, value);
    }

    get(key) {
        return this.store.get(key);
    }
}

/**
 * @class ActionEndpointList
 * @description represents the list of url present
 */
export class ActionEndpointList extends Map {
}

class ConfigurationContainer extends Component {
    state = {
        loading: true,
        error: false,
        configurations: {},
        createOpen: false,
    }

    handleCreateDialog = config => {
        this.setState({ createOpen: true })
    }

    handleDeleteDialog = config => {
        this.setState({ deleteOpen: true })
    }

    handleUpdateDialog = config => {
        this.setState({ updateOpen: true })
    }

    buildActions(urls, actions) {
        const configActions = {}

        for (let action of actions) {
            switch (action) {
                case 'create':
                    configActions.create = {
                        type: 'button',
                        name: 'Create',
                        callback: this.handleCreateDialog,
                    };

                    break;

                case 'delete':
                    configActions.delete = ({
                        type: 'button',
                        name: 'Delete',
                        callback: this.handleDeleteDialog,
                    });
                    break;

                case 'update':
                    configActions.update = {
                        type: 'button',
                        name: 'Update',
                        callback: this.handleUpdateDialog,
                    }
                    break;

                default:
            }
        }

        configActions.select = {
            type: 'button',
            name: 'Select',
            callback: this.handleSelect,
        }

        return configActions;
    }

    handleSelect = config => {
        this.setState({ selected: config })
    }

    fetchConfigurations() {
        const getUrl = this.props.urls.get('GET');

        AuthorizedApi.authApi.makeRequest(getUrl, {}, {
            onSuccess: list => {
                this.setState({ configurations: list, loading: false, error: false })
            },
            onFailed: error => {
                this.setState({ error, loading: false })
            }
        })
    }

    componentDidMount() {
        this.fetchConfigurations();
    }

    handleClose = prop => {
        return () => {
            this.setState({ [prop]: false })
        }
    }

    handleSubmit = config => {
        const options = {
            method: 'POST',
            body: JSON.stringify(config),
        }
        console.log("CREATING ", config)
        this.setState({ working: true });
        AuthorizedApi.authApi.makeRequest(this.props.urls.get('POST'), options, {
            onSuccess: res => {
                this.setState({ working: false })
                this.refresh()
            },
            onFailed: error => {
                this.setState({ error, working: false })
            }
        });
    }

    handleUpdate = config => {
        const options = {
            method: 'PUT',
            body: JSON.stringify(config)
        }
        console.log("UPDATING ", config);
        this.setState({ working: true });
        AuthorizedApi.authApi.makeRequest(this.props.urls.get('PUT')+'/'+config.id, options, {
            onSuccess: res => {
                this.setState({ working: false })
                this.refresh()
            },
            onFailed: error => {
                this.setState({ error, working: false })
            }
        });
    }

    handleDelete = config => {
        this.setState({ working: true })
        AuthorizedApi.authApi.makeRequest(this.props.urls.get('DELETE')+'/'+config.id, {
            method: 'DELETE'
        }, {
            onSuccess: res => {
                this.setState({ working: false })
                this.refresh()
            },
            onFailed: error => {
                this.setState({ error, working: false })
            }
        });
    }

    refresh() {
        this.setState({ loading: true, error: false, configuratoins: false })
        this.fetchConfigurations()
    }

    render() {
        const { classes, urls, actions, schema } = this.props;

        const configActions = this.buildActions(urls, actions);
        return (
            <div className={classes.root}>
                <Typography type='headline'>
                    {schema.headline}
                </Typography>
                {this.state.working && <LinearProgress /> }
                <LoadErrComponent error={this.state.error} loading={this.state.loading}>
                    <Configuration actions={configActions} columns={schema.columns} configurations={this.state.configurations} onSelect={this.handleSelect}/>
                    <CreateConfigurationDialog open={this.state.createOpen} onClose={this.handleClose('createOpen')} schema={schema} onSubmit={this.handleSubmit} />
                    <UpdateConfigurationDialog open={this.state.updateOpen} onClose={this.handleClose('updateOpen')} schema={schema} onSubmit={this.handleUpdate} configuration={this.state.selected} />
                    <DeleteConfigurationDialog open={this.state.deleteOpen} onCancel={this.handleClose('deleteOpen')} onSuccess={this.handleDelete} schema={schema} configuration={this.state.selected} />
                </LoadErrComponent>
            </div>
        )
    }
}

export default withStyles(styles)(ConfigurationContainer);
