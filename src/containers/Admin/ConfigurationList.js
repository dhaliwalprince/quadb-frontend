import React, { Component } from 'react';
import withStyles from 'material-ui/styles/withStyles';
import Loader from '../../components/Loader';
import Error from '../../components/Error';
import ListItem from 'material-ui/List/ListItem';
import ListItemText from 'material-ui/List/ListItemText';
import { Link } from 'react-router-dom';
import List from 'material-ui/List/List';
import { AuthorizedApi } from '../../globals/authorization';

const styles = theme => ({

})

class Configurations extends Component {
    render() {
        const { classes, configurations } = this.props;

        return (
            <List className={classes.configurationListRoot}>
                {
                    configurations.map((configuration, id) => {
                        return (
                            <ListItem key={id} button component={Link} to={'/manage/meta/'+configuration.id} divider>
                                <ListItemText primary={configuration.headline || '<'+configuration.name+'>'} />
                            </ListItem>
                        )
                    })
                }
            </List>
        )
    }
}

class ConfigurationList extends Component {
    state = {
        error: false,
        loading: false,
    }

    componentDidMount() {
        AuthorizedApi.authApi.makeRequest('/api/meta', {}, {
            onSuccess: configurations => {
                this.setState({ configurations, loading: false, error: false })
            },
            onFailed: error => {
                this.setState({ loading: false, error })
            }
        })
    }

    handleRetry = () => {
        this.setState({ loading: true, error: false })
        this.componentDidMount();
    }
    
    render() {
        const { classes } = this.props;

        return (
            <div className={classes.root}>
                {this.state.loading && <Loader classes={classes} /> }
                { this.state.error && <Error error={this.state.error} onRetry={this.handleRetry} /> }
                { this.state.configurations && <Configurations classes={classes} configurations={this.state.configurations} /> }
            </div>
        )
    }
}

export default withStyles(styles)(ConfigurationList);
