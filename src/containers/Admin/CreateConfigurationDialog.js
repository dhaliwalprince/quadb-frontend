import React, { Component } from 'react';
import withStyles from 'material-ui/styles/withStyles';
import Dialog from 'material-ui/Dialog/Dialog';
import DialogTitle from 'material-ui/Dialog/DialogTitle';
import DialogContent from 'material-ui/Dialog/DialogContent';
import DialogActions from 'material-ui/Dialog/DialogActions';
import Button from 'material-ui/Button/Button';
import Form from '../Form';
import Typography from 'material-ui/Typography/Typography';
import { rectifyMeta } from './UpdateConfigurationDialog';

const styles = theme => ({

})

class CreateConfigurationDialog extends Component {    
    render() {
        const { classes, onSubmit, schema, open, onClose} = this.props;

        return (
            <div className={classes.root}>
                <Dialog open={open}>
                    <DialogTitle>Create</DialogTitle>
                    <DialogContent>
                        <Typography type='caption'>
                            You can leave auto generated fields like id, etc empty.
                        </Typography>
                        <Form
                            columns={schema.columns.map(column => ({ ...column, field: column.key }))}
                            submitLabel={'Create'}
                            disabled={false}
                            onSubmit={values => onSubmit(rectifyMeta(schema, values))}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={onClose}>
                        Cancel
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

export default withStyles(styles)(CreateConfigurationDialog);