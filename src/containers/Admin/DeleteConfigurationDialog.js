import React from 'react';
import Dialog from 'material-ui/Dialog/Dialog';
import DialogTitle from 'material-ui/Dialog/DialogTitle';
import DialogContent from 'material-ui/Dialog/DialogContent';
import DialogContentText from 'material-ui/Dialog/DialogContentText';
import DialogActions from 'material-ui/Dialog/DialogActions';
import Button from 'material-ui/Button/Button';
import withStyles from 'material-ui/styles/withStyles';

const styles = theme => ({

})

function DeleteConfigurationDialog({ classes, schema, open, onSuccess, onCancel, configuration }) {
    return (
        <Dialog open={open}>
            <DialogTitle>Delete {schema.name || 'Configuration'}</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Are you sure?
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={onCancel}>
                Cancel
                </Button>
                <Button onClick={() => onSuccess(configuration)}>
                Confirm
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default withStyles(styles)(DeleteConfigurationDialog);
