import React, { Component, Fragment } from 'react';
import withStyles from 'material-ui/styles/withStyles';
import FileUploader from './FileUploader';
import { AuthorizedApi } from '../../globals/authorization';
import LinearProgress from 'material-ui/Progress/LinearProgress';

const styles = theme => ({
    root: {
        padding: theme.spacing.unit *2
    }
})

const uploadFile = (file, i) => {
    const formData = new FormData()
    formData.append("file[]", file, file.name)
    return formData
}

const makeRequest = (onProgress, onComplete, onError, data, i) => {
    const request = new XMLHttpRequest()
    request.open("POST", "/api/filestore/upload", true)

    request.setRequestHeader('Authorization', `Bearer ${AuthorizedApi.authApi.getToken()}`)
    request.onload = event => {
        console.log(request.status)
        if (request.status === 200) {
            onComplete()
        } else {
            onError()
        }
    }
    request.send(data);

    request.onerror = event => {
        onError(event)
    }
    request.onprogress = event => {
        if (event.lengthComputable) {
            onProgress(event.loaded / event.total)
        }
    }
    return request;
}

class UploaderComponent extends Component {
    componentDidMount() {
        const { files } = this.props;
        const requests = files.map(uploadFile).map(makeRequest.bind(null, this.handleProgress, this.handleComplete, this.handleError))
    }

    handleProgress = percent => {
        this.props.onProgress(percent);
    }

    handleComplete = () => {
        this.props.onComplete(100);
    }

    handleError = err => {
        this.props.onError(err);
    }
    render() {
        return null
    }
}

class FileManager extends Component {
    state = {
        uploading: false,
        uploadProgress: 0,
    }

    handleUpload = (files) => {
    this.setState({ uploadFiles: files, uploading: true, })
    }

    handleProgress = progress => {
        console.log("Progress: ", progress)
        this.setState({ uploadProgress: progress*100 })
    }

    handleComplete = progress => {
        console.log("Completed")
        this.setState({ uploadProgress: progress, uploading: false })
    }

    render() {
        const { classes } = this.props;
        return (
            <div className={classes.root}>
                <FileUploader onUpload={this.handleUpload} />
                {
                    this.state.uploading && <UploaderComponent files={this.state.uploadFiles} onError={console.log} onProgress={this.handleProgress} onComplete={this.handleComplete} />
                }
                {
                    this.state.uploading && <LinearProgress mode="determinate" value={this.state.uploadProgress} />
                }
            </div>
        )
    }
}

export default withStyles(styles)(FileManager);
