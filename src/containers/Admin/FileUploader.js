import React, { Component, Fragment } from 'react';
import withStyles from 'material-ui/styles/withStyles';
import Dropzone from 'react-dropzone';
import Typography from 'material-ui/Typography/Typography';
import List from 'material-ui/List/List';
import ListSubheader from 'material-ui/List/ListSubheader';
import ListItemText from 'material-ui/List/ListItemText';
import ListItemAvatar from 'material-ui/List/ListItemAvatar';
import Avatar from 'material-ui/Avatar/Avatar';
import ListItem from 'material-ui/List/ListItem';
import Button from 'material-ui/Button/Button';

const styles = theme => ({

})

class FileUploader extends Component {
    state = {
        files: []
    }

    handleDrop(files) {
        this.setState({ files: [ ...this.state.files, ...files ] })
    }
    
    render() {
        const {classes} = this.props;
        
        return (
            <div className={classes.root}>
                <Dropzone onDrop={files => this.handleDrop(files)}>
                    <Typography type='caption' align='center'>Drop files here.</Typography>
                </Dropzone>

                { this.state.files &&
                <Fragment>
                    <List>
                        <ListSubheader>Files to be uploaded: </ListSubheader>
                        {
                            this.state.files.map((file, id) => (
                                <ListItem key={id}>
                                    <ListItemAvatar><Avatar alt={file.name[0]} src={file.preview} /></ListItemAvatar> 
                                    <ListItemText primary={file.name} secondary={file.type} />
                                </ListItem>
                            ))
                        }
                    </List>
                    <div>
                        <Button color='primary' raised onClick={() => this.props.onUpload(this.state.files)}>
                            Upload
                        </Button>
                    </div>
                </Fragment>
                }
            </div>
        )
    }
}

export default withStyles(styles)(FileUploader);
