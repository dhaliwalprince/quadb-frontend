import React, { Component } from 'react';
import withStyles from 'material-ui/styles/withStyles';
import Loader from '../../components/Loader';
import Error from '../../components/Error';
import ConfigurationContainer, { ActionEndpointList } from './ConfigurationContainer';
import { AuthorizedApi } from '../../globals/authorization';

const styles = theme => ({
    root: {
        padding: theme.spacing.unit * 2,
        flex: 10,
    }
})

class MetaManager extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            error: false,
            configuration: false,
            id: props.match.params.metaId,
        }
    }

    fetchMeta() {
        const id = this.state.id;
        AuthorizedApi.authApi.makeRequest('/api/meta/'+id, {}, {
            onSuccess: meta => {
                this.setState({ error: false, loading: false, configuration: meta })
            },
            onFailed: error => {
                this.setState({ error, loading: false, configuration:false })
            }
        })
    }

    componentDidMount() {
        this.fetchMeta()
    }

    componentWillReceiveProps(props) {
        if (this.state.id !== props.match.params.metaId) {
            this.setState({ id: props.match.params.metaId, loading: true, error: false, configuration: false })
        }
    }

    componentDidUpdate(oldProps, oldState) {
        if (oldState.loading === this.state.loading && this.state.error === oldState.error)
            this.fetchMeta();
    }

    render() {
        const { classes } = this.props;

        let actions = [];
        let urls = new ActionEndpointList()

        if (this.state.configuration) {
            this.state.configuration.urls.forEach((url, id) => {
                switch (url.type) {
                    case 'POST':
                        actions.push('create')
                        break;
                    case 'DELETE':
                        actions.push('delete')
                        break;
                    case 'PUT':
                        actions.push('update')
                        break;

                    case 'GET':
                    default:
                        break;
                }
                urls.set(url.type, url.endpoint);
            })
        }
        return (
            <div className={classes.root}>
                { this.state.loading && <Loader classes={classes} />}
                {this.state.error && <Error error={this.state.error} />}
                { this.state.configuration && 
                    <ConfigurationContainer actions={actions} urls={urls} schema={this.state.configuration} />
                }
            </div>
        )
    }
}

export default withStyles(styles)(MetaManager);
