import React, { Component } from 'react';
import withStyles from 'material-ui/styles/withStyles';
import order_manager from '../../globals/admin-api/order-manager';
import LoadErrComponent from '../../components/LoadErrComponent'
import OrderList from '../../components/admin/OrderList';

const styles = theme => ({
})

class OrderManager extends Component {
    state = {
        orders: null,
        loading: true,
    }

    componentDidMount() {
        this.manager = new order_manager();

        this.manager.list({
            filter: 'created',
            onSuccess: orders => {
                this.setState({ orders, error: false, loading: false  })
            },
            onFailed: error => {
                this.setState({ error, loading: false })
            }
        })
    }

    render() {
        const {classes} = this.props;

        return (
            <div className={classes.root}>
                <LoadErrComponent
                    error={this.state.error}
                    loading={this.state.loading}
                    component={
                        <OrderList orders={this.state.orders} onAccept={this.handleAccept} onReject={this.handleReject} onDelivered={this.handleDelivered} />
                    }
                />
            </div>
        )
    }
}

export default withStyles(styles)(OrderManager);
