import React, { Component, Fragment } from 'react';
import withStyles from 'material-ui/styles/withStyles';
import FormControl from 'material-ui/Form/FormControl';
import InputLabel from 'material-ui/Input/InputLabel';
import Input from 'material-ui/Input/Input';
import { AuthorizedApi } from './../../globals/authorization';
import Select from 'react-select';

import 'react-select/dist/react-select.css';
import Button from 'material-ui/Button/Button';
import LinearProgress from 'material-ui/Progress/LinearProgress';
import Typography from 'material-ui/Typography/Typography';
import { totalQuantity } from '../../subapps/AdminRequestsContainer';
import List from 'material-ui/List/List';
import ListSubheader from 'material-ui/List/ListSubheader';
import ListItemText from 'material-ui/List/ListItemText';
import ListItem from 'material-ui/List/ListItem';
import Error from '../../components/Error';

const styles = theme => ({
    root: {
        padding: theme.spacing.unit *2
    },
    form: {
        display: 'flex',
        flexDirection: 'column',
    },
    formControl: {
        marginBottom: theme.spacing.unit*2
    }
})

export function toOneLine(sizeList) {
    return sizeList.map(size => `${size.code}: ${size.quantity}`).join(', ')
}

export function getQuery(name) {
    const q = window.location.search.slice('?'.length).split('&').find(element => element.startsWith(name))
    return q.split('=')[1]
}

class RequestDetails extends Component {
    state = {
        loading: true,
        loadingQuotations: true,
    }
    
    componentDidMount() {
        AuthorizedApi.authApi.makeRequest('/api/orders/requests/'+this.props.requestId+'/quotations', {}, {
            onSuccess: quotations => {
                this.setState({ quotations, loadingQuotations: false, errorQuotation: false })
            },
            onFailed: error => {
                this.setState({ loadingQuotations: false, errorQuotation: error })
            }
        });

        AuthorizedApi.authApi.makeRequest('/api/orders/requests/'+this.props.requestId, {}, {
            onSuccess: request => {
                this.setState({ loading: false, request, error: false })
            },
            onFailed: error => {
                this.setState({ error, loading: false })
            }
        })
    }

    componentWillReceiveProps() {
        this.componentDidMount();
    }
    render() {
        const { classes} = this.props;
        let q = 0;
        if (!this.state.loading && !this.state.error) {
            q = totalQuantity(this.state.request.size_list);
        }
        return (
            <div className={classes.details}>
                { !this.state.loading &&
                    <Fragment>
                        <Typography type='caption'>Request no. { this.state.request && this.state.request.id }, Quantity: {q}</Typography>
                    </Fragment>
                }
                {
                   !this.state.loadingQuotations &&
                        <Fragment>
                            <List>
                                <ListSubheader>Quotations</ListSubheader>
                                { this.state.quotations && this.state.quotations.map((quote, id) => {
                                    console.log(quote);
                                    return (
                                        <ListItem>
                                            <ListItemText primary={quote.cloth_material.name + ' | ' + quote.category.name} secondary={`${quote.price_per_item} | Total: ${q * parseInt(quote.price_per_item)}`} />
                                        </ListItem>
                                    )
                                })}
                            </List>
                        </Fragment>
                }
            </div>
        )
    }
}

class QuotationForm extends Component {
    state = {
        clothOptions: [],
        categoryOptions: [],
        values: {}
    }

    componentDidMount() {
        AuthorizedApi.authApi.makeRequest('/api/catalog/materials', {}, {
            onSuccess: clothOptions => {
                this.setState({ clothOptions: clothOptions.map((option, id) => ({ label: option.name, value: option.id }))})
            },
            onError: error => {
                this.setState({ error })
            }
        })
        AuthorizedApi.authApi.makeRequest('/api/catalog/categories', {}, {
            onSuccess: clothOptions => {
                this.setState({ categoryOptions: clothOptions.map((option, id) => ({ label: option.name, value: option.id }))})
            },
            onError: error => {
                this.setState({ error })
            }
        })
    }

    handleChange(prop) {
        return ({ target }) => {
            this.setState({ values: { ...this.state.values, [prop]: target.value }})
        }
    }

    handleClothMaterial = material => {
        this.setState({ values: { ...this.state.values, cloth_material_id: material.value }})
    }

    handleCategory = cat => {
        this.setState({ values: { ...this.state.values, category_id: cat.value }})        
    }

    componentDidCatch(error) {
        this.setState({ error })
    }

    handleSubmit = event => {
        event.preventDefault()
        event.stopPropagation()
        this.state.values.price_per_item = parseFloat(this.state.values.price_per_item);
        this.state.values.request_id = parseInt(getQuery('requestId'));
        this.setState({ submitting: true })
        const options = {}
        options.body = JSON.stringify(this.state.values)
        options.method = 'POST'
        AuthorizedApi.authApi.makeRequest('/api/orders/requests/'+getQuery('requestId')+'/quotations', options, {
            onSuccess: () => {
                this.setState({ submitting: false, values: {}, error: false, update: !this.state.update })
            },
            onFailed: error => {
                this.setState({ submitting: false, error })
            }
        })
    }

    render() {
        const { classes } = this.props;

        return (
            <form className={classes.form} onSubmit={this.handleSubmit}>
                { this.state.submitting && <LinearProgress /> }
                { this.state.error && <Error error={this.state.error} classes={classes} />}
                <FormControl className={classes.formControl}>
                    <RequestDetails requestId={getQuery('requestId')} classes={classes} update={this.state.update}/>
                </FormControl>
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor={'price_per_item'}>Price per item</InputLabel>
                    <Input type="text" value={this.state.values['price_per_item']} name='price_per_item' onChange={this.handleChange('price_per_item')} />
                </FormControl>
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor={'cloth_material_id'}>Cloth material</InputLabel>
                    <Select placeholder={'Choose Cloth Material'} name='cloth_material_id' value={this.state.values['cloth_material_id']} options={this.state.clothOptions} fluid selection onChange={this.handleClothMaterial} />
                </FormControl>
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor={'category_id'}>Category</InputLabel>
                    <Select placeholder={'Choose category'} value={this.state.values['category_id']} name='category_id' options={this.state.categoryOptions} fluid selection onChange={this.handleCategory} />
                </FormControl>
                <FormControl className={classes.formControl}>
                    <Button color='primary' raised type='submit'>Submit</Button>
                </FormControl>
            </form>
        )
    }
}

class QuotationGenerator extends Component {
    render() {
        const { classes } = this.props;
        const requestId = getQuery('requestId');
        return (
            <div className={classes.root}>
                 Generate quotations for request #{requestId}

                 <QuotationForm classes={classes} />
            </div>
        )
    }
}

export default withStyles(styles)(QuotationGenerator);
