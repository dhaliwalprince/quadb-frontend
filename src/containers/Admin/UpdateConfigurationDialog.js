import React, { Component } from 'react';
import withStyles from 'material-ui/styles/withStyles';
import Dialog from 'material-ui/Dialog/Dialog';
import DialogTitle from 'material-ui/Dialog/DialogTitle';
import DialogContent from 'material-ui/Dialog/DialogContent';
import DialogActions from 'material-ui/Dialog/DialogActions';
import Button from 'material-ui/Button/Button';
import Form, { FormInput } from '../Form';
import Typography from 'material-ui/Typography/Typography';

const styles = theme => ({
    formInput: {
        margin: theme.spacing.unit,
    }
})

export function rectifyMeta(schema, meta) {
    schema.columns.forEach(column => {
        if (typeof meta[column.key] === 'undefined') {
            return;
        }

        if (column.type === 'number') {
            if (typeof meta[column.key] === 'string') {
                meta[column.key] = parseInt(meta[column.key])
            }
        }
    })

    return meta;
}

class UpdateForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            values: props.configuration,
            errors: {},
        }
    }

    checkErrors() {
        const errors = {}
        this.props.schema.columns.forEach((column, id) => {
            const value = this.state.values[column.key];
            if (column.required && (typeof value === 'undefined'
                                    || (value.length === 0))) {
                errors[column.key] = {
                    message: 'Missing column: ' + column.name,
                }
                return;
            }
        })

        if (Object.keys(errors).length > 0) {
            this.setState({ errors })
            return true;
        } else {
            return false;
        }
    }

    handleSubmit = (event) => {
        event.preventDefault()
        event.stopPropagation()

        if (this.checkErrors()) {
            return;
        }

        this.props.onSubmit(rectifyMeta(this.props.schema, this.state.values));
    }
    
    handleChange(field) {
        return ({ target }) => {
            const values = { ...this.state.values, [field]: target.value }
            this.setState({ values })
        }
    }
    
    render() {
        const { classes, schema } = this.props;

        return (
            <form onSubmit={this.handleSubmit}>
                {
                    schema.columns.map((column, id) => {
                        return (
                            <FormInput
                                classes={classes}
                                column={column}
                                onChange={this.handleChange(column.key)}
                                value={this.state.values[column.key]}
                                disabled={column.editable}
                                error={this.state.errors[column.key]}
                            />
                        )
                    })
                }
                
                <div className={classes.submit}>
                    <Button type="submit" raised>
                    Save
                    </Button>
                </div>
            </form>
        )
    }
}

class UpdateConfigurationDialog extends Component {    
    render() {
        const { classes, onSubmit, configuration, schema, open, onClose} = this.props;

        return (
            <div className={classes.root}>
                <Dialog open={open}>
                    <DialogTitle>Update</DialogTitle>
                    <DialogContent>
                        <Typography type='caption'>
                            You can leave auto generated fields like id, etc empty.
                        </Typography>
                        <UpdateForm classes={classes} configuration={configuration} schema={schema} onSubmit={onSubmit} />
                        <div className={classes.submit}>
                            <Button onClick={onClose}>
                            Cancel
                            </Button>
                        </div>
                    </DialogContent>
                </Dialog>
            </div>
        )
    }
}

export default withStyles(styles)(UpdateConfigurationDialog);