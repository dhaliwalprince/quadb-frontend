import React, { Component, Fragment } from 'react';

import UserSchema from '../../schemas/user';
import withStyles from 'material-ui/styles/withStyles';
import ConfigurationContainer, { ActionEndpointList } from './ConfigurationContainer';

const styles = theme => ({

})

class UserManager extends Component {
    render() {
        const urls = new ActionEndpointList();
        Object.keys(UserSchema.urls).forEach(key => {
            urls.set(key, UserSchema.urls[key])
        })
        return (
            <Fragment>
                <ConfigurationContainer schema={UserSchema} urls={urls} actions={ ['delete']} />
            </Fragment>
        )
    }
}

export default withStyles(styles)(UserManager);
