import AlertList from '../components/AlertList';
import { removeAlert } from '../actions/globals';
import { connect } from 'react-redux';

const mapStateToProps = state => {
    return {
        alerts: state.alerts
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onDismiss: (index) => {
            dispatch(removeAlert(index))
        }
    }
}

const AlertContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(AlertList);

export default AlertContainer;
