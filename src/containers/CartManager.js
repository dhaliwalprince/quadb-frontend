import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import Loader from '../components/Loader';
import Cart from '../components/Cart';
import UserApi from '../globals/user'
import Error, { ShortError } from '../components/Error';
import withRouter from 'react-router-dom/withRouter';
import Button from 'material-ui/Button/Button';
import { AuthorizedApi } from '../globals/authorization';
import { CART_URL } from '../globals/urls';
import LinearProgress from 'material-ui/Progress/LinearProgress';


const styles = theme => ({
    progress: {

    }
})

class CartManager extends Component {
    state = {
        loading: true,
    }

    componentDidMount() {
        const user = UserApi.GetInstance()
        user.orders.cart({
            onSuccess: cart => {
                this.setState({ loading: false, cart, error: false })
            },
            onFailed: error => {
                this.setState({ error })
            }
        })
    }

    handleEdit = item => {
        this.props.history.push('/orders/' + item.id + '/edit')
    }

    handlePlace = item => {
        this.props.history.push('/orders/' + item.id + '/place')
    }

    handleRemove = item => {
        const user = UserApi.GetInstance()
        user.orders.remove(item, {
            onSuccess: () => {
                this.setState({ removing: false, loading: true })
                this.componentDidMount()
            },
            onFailed: error => {
                this.setState({ removing: false, shortError: error })
            }
        })

        this.setState({ removing: true })
    }

    handleDummy = () => {
        AuthorizedApi.authApi.makeRequest(CART_URL, { method: 'POST', body: JSON.stringify({
            "user_id": 1,
            "order_details": {
                "design_id": 2,
                "cloth_material_id": 2,
                "category_id": 1,
                "item_price": 500,
                "quantity": 5
            }
        }) }, {
            onSuccess: (res) => {
                this.setState({ adding: false, loading: true })
                this.componentDidMount()
            },
            onFailed: error => {
                this.setState({ shortError: error })
            }
        })
        this.setState({ adding: true })
    }
    
    render() {
        const { classes } = this.props;
        if (this.state.error) {
            return <Error error={this.state.error} />
        }
        return (
            <div>
                { this.state.shortError && <ShortError error={this.state.shortError} /> }
                { this.state.removing && <LinearProgress /> }
                {
                    this.state.loading ? <Loader classes={classes} /> :
                        <Cart cart={this.state.cart}
                              onEdit={this.handleEdit}
                              onPlace={this.handlePlace}
                              onRemove={this.handleRemove} />
                }

            </div>
        )
    }
}

export default withRouter(withStyles(styles)(CartManager));

