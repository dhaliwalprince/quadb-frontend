import React, { Component } from 'react';
import withStyles from 'material-ui/styles/withStyles';
import Paper from 'material-ui/Paper/Paper';
import Typography from 'material-ui/Typography/Typography';
import FormControl from 'material-ui/Form/FormControl';
import DialogInput from './DialogInput';
import Button from 'material-ui/Button/Button';
import FormHelperText from 'material-ui/Form/FormHelperText';
import Loader from '../components/Loader';
import UserApi from '../globals/user'
import LinearProgress from 'material-ui/Progress/LinearProgress';
import ListItem from 'material-ui/List/ListItem';
import ListItemText from 'material-ui/List/ListItemText';
import InputLabel from 'material-ui/Input/InputLabel';
import Input from 'material-ui/Input/Input';
import TextField from 'material-ui/TextField/TextField';
import SizeChooserDialog from './SizeChooserDialog';
import AddressChooser from './AddressChooser';

export function isInvalidSize(sizes) {
    if (typeof sizes === 'undefined')
        return true;

    let allZero = true;
    for (let code in sizes) {
        if (sizes[code] > 0) {
            allZero = false
        }
    }

    return allZero
}

export function isInvalidAddress(address) {
    return typeof address === 'undefined';
}
export function toList(sizes) {
        if (typeof sizes === 'undefined') {
            return 'Enter the size list.'
        }

        let result = '', count = 0;
        for (let key in sizes) {
            if (sizes[key] === 0) {
                continue;
            }

            count++;

            if (count !== 1) {
                result += ', '
            }

            result += `${key} - ${sizes[key]}`            
        }

        return result || 'Enter the size list.';
    }


const styles = theme => ({
    root: {
        maxWidth: 480,
        display: 'block',
        margin: 'auto'
    },
    paper: {
    },
    padding: {
        padding: theme.spacing.unit * 2,
    },
    options: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    orderInfo: {
        paddingBottom: theme.spacing.unit,
    },
    submitButton: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
    },
    input: {
        marginLeft: theme.spacing.unit*2,
        marginRight: theme.spacing.unit*2,
    }
})

class Checkout extends Component {
    commentsNode = {
        value: ''
    }

    constructor(props) {
        super(props)

        console.log(props.match.params)
        this.state = {
            data: {
                quantity: 5,
                designId: props.match.params.designId
            },
            open: {},
            locked: true
        }
    }

    componentDidMount() {
        const user = UserApi.GetInstance()
        user.designs.findByHash(this.state.data.designId, {
            onSuccess: (design) => {
                this.setState({ design, locked: false, data: { ...this.state.data, designId: design.name } })
            },
            onFailed: (error) => {
                this.setState({ error, locked: false })
            }
        })
    }

    handleChange(prop) {
        return (value) => {
            if (typeof value === 'undefined')
                return

            if (prop === 'quantity') {
                try {
                    value = parseInt(value)
                } catch (err) {
                    this.setState(() => ({ error: 'Quantity should be a valid positive integer.'}))
                }
            }
            console.log(value)
            const data = { ...this.state.data }
            data[prop] = value
            const open = { ...this.state.open }
            open[prop] = false
            this.setState(() => ({ data, open }))
        }
    }

    handleClick(prop) {
        return () => {
            const open = { ...this.state.open }
            open[prop] = true
            this.setState(() => ({ open }))
        }
    }

    handleSubmit = () => {
        const data = this.state.data;
        const user = UserApi.GetInstance()

        if (typeof this.state.sizes === 'undefined' || isInvalidSize(this.state.sizes)) {
            this.setState({ error: { message: 'Invalid quantity.' }})
            return
        }

        if (typeof this.state.address === 'undefined' || isInvalidAddress(this.state.address)) {
            this.setState({ error: { message: 'Please select delivery address.' }})
            return
        }
        const sizes = []
        for (const code in this.state.sizes) {
            sizes.push({ code, quantity: this.state.sizes[code] })
        }
        user.orders.createRequest({
            design_id: this.state.design.id,
            user_comments: this.commentsNode.value,
            address_id: this.state.address.id,
            size_list: sizes,
        }, {
            onSuccess: res => {
                this.setState({ submitting: false, error: false, placed: true })
            },
            onFailed: error => {
                this.setState({ submitting: false, error })
            }
        })
        this.setState({ submitting: true })
    }

    handleComments = ({target}) => {
        this.commentsNode = {
            value: target.value
        }
    }

    handleSizeListChange = (sizes) => {
        this.setState({ sizes, open: { ...this.state.open, sizesDialog: false } })
    }

    toAddress(address) {
        if (typeof address === 'undefined')
            return 'Select address.'
        
        return `${address.line1}, ${address.line2}, ${address.city}, ${address.state}, ${address.pincode}, ${address.mobile}`
    }

    handleAddressChange = address => {
        this.setState({ address, open: { ...this.state.open, addressDialog: false }})
    }
    render() {
        const { classes, match } = this.props;
        if (this.state.placed) {
            return (
                <div className={classes.root}>
                    <Paper className={classes.paper}>
                        <Typography align='center'>
                            Successfully submitted order.
                        </Typography>
                    </Paper>
                </div>
            )
        }
        this.designId = match.params.designId
        return (
            <div className={classes.root}>
                <Paper className={classes.paper}>
                    { this.state.locked && <LinearProgress /> }
                    <Typography type='headline' className={classes.padding}>
                    Checkout
                    </Typography>
                    <div className={classes.options}>
                        <FormControl className={classes.formControl}>
                            <ListItem button disabled>
                                <ListItemText primary="Design" secondary={this.state.data['designId']} />
                            </ListItem>
                        </FormControl>
                        <FormControl className={classes.formControl}>
                            <ListItem button onClick={this.handleClick('sizesDialog')}>
                                <ListItemText primary={'Sizes'} secondary={toList(this.state.sizes)} />
                            </ListItem>
                        </FormControl>
                        <FormControl className={classes.formControl}>
                            <ListItem button onClick={this.handleClick('addressDialog')}>
                                <ListItemText primary={'Delivery address'} secondary={this.toAddress(this.state.address)} />
                            </ListItem>
                        </FormControl>
                        <TextField multiline label={'Comments'} onChange={this.handleComments} className={classes.input} ref={node => this.commentsNode = node} />
                    </div>
                    <div className={classes.padding}>
                        <FormHelperText className={classes.orderInfo}>
                            You can submit your order. Price will be emailed to you after reviewing.
                        </FormHelperText>
                        <div className={classes.submitButton}>
                            <Button disabled={this.state.submitting || this.state.locked} compact color='primary' raised onClick={this.handleSubmit}>
                                Submit
                            </Button>
                            {
                                this.state.error &&  
                                <FormHelperText className={classes.orderInfo} type={'error'}>
                                {this.state.error.message}
                                </FormHelperText>
                            }
                            {
                                this.state.submitting &&
                                    <Loader classes={classes} />
                            }
                        </div>
                    </div>
                    <SizeChooserDialog open={this.state.open.sizesDialog} onChange={this.handleSizeListChange} />
                    <AddressChooser open={this.state.open.addressDialog} onChange={this.handleAddressChange} />
                </Paper>
            </div>
        )
    }
}

export default withStyles(styles)(Checkout)
