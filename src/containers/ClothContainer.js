import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import Loader from '../components/Loader';
import ClothStand from '../components/ClothStand';
import api from '../globals/common'

const styles = theme => ({

})

class ClothContainer extends Component {
    state = {
        loading: true
    }

    componentDidMount() {
        api.clothes({
            onSuccess: clothes => this.setState({ clothes, loading: false })
        })
    }
    
    render() {
        const { classes } = this.props;
        return (
            <div className={classes.root}>
                {
                    this.state.loading ? <Loader type='linear' classes={classes} /> : <ClothStand clothes={this.state.clothes} />
                }
            </div>
        )
    }
}

export default withStyles(styles)(ClothContainer)
