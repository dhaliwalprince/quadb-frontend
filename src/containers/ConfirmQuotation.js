import React, { Component } from 'react';
import withStyles from 'material-ui/styles/withStyles';
import LoadErrComponent from '../components/LoadErrComponent';
import { AuthorizedApi } from '../globals/authorization';
import Typography from 'material-ui/Typography/Typography';
import { toOneLine } from './Admin/QuotationGenerator';
import Divider from 'material-ui/Divider/Divider';
import DialogActions from 'material-ui/Dialog/DialogActions';
import Button from 'material-ui/Button/Button';

const styles = theme => ({
    root: {
        maxWidth: 600,
        margin: 'auto',
        display: 'block'
    },
    divider: {
        marginTop: theme.spacing.unit,
        marginBottom: theme.spacing.unit,
    }
})

class ConfirmQuotation extends Component {
    state = {
        loading: true,
        error: false,
    }
    
    componentDidMount() {
        AuthorizedApi.authApi.makeRequest('/api/quotations/'+this.props.match.params.quotationId, {}, {
            onSuccess: quote => {
                this.setState({ loading: false, error: false, quote })
            },
            onFailed: error => {
                this.setState({ loading: false, error })
            }
        })
    }
    
    render() {
        const { classes } = this.props;
        const quote = this.state.quote;
        return (
            <LoadErrComponent loading={this.state.loading} error={this.state.error}>
            {
                this.state.quote && 
                <div className={classes.root}>
                    <Typography type={'button'}>
                        Confirm order
                    </Typography>
                    <Divider className={classes.divider} />

                    <Typography>
                        <b>Quotation Reference No.:</b>&nbsp;{quote.id}
                    </Typography>
                    <Typography>
                        <b>Delivery Address:</b>&nbsp;{`${quote.request.address.line1}, ${quote.request.address.line2}, ${quote.request.address.city}, ${quote.request.address.state}, ${quote.request.address.pincode}`}
                    </Typography>
                    <Typography>
                        <b>Total Quantity:</b>&nbsp;{`${quote.quantity} (${toOneLine(quote.request.size_list)})`}
                    </Typography>
                    <Typography>
                        <b>Price Per Item:</b>&nbsp;{`Rs. ${quote.price_per_item}`}
                    </Typography>
                    <Typography>
                        <b>Total Price:</b>&nbsp;{`Rs. ${quote.price_per_item * quote.quantity}`}
                    </Typography>

                    <Typography>
                        <b>Cloth Material:</b>&nbsp;{quote.cloth_material.name}
                    </Typography>
                    <Typography>
                        <b>Type:</b>&nbsp;{quote.category.name}
                    </Typography>
                    <Typography>
                        <b>Design:</b>&nbsp;{quote.request.design.name}(ID: {quote.request.design.id})
                    </Typography>

                    <DialogActions>
                        <Button raised color='primary' type="link" href="/payment">
                        Proceed to payment
                        </Button>
                    </DialogActions>
                </div>
            }
            </LoadErrComponent>
        )
    }
}

export default withStyles(styles)(ConfirmQuotation);
