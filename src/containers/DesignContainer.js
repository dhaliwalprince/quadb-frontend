import React, { Component } from 'react'
import { withStyles } from 'material-ui/styles';
import Loader from '../components/Loader';
import DesignList from '../components/DesignList';
import UserApi from '../globals/user'
import { ShortError } from '../components/Error';

const styles = theme => ({
    root: {
        maxWidth: '768px',
        margin: 'auto',
        textAlign: 'center'
    }
})

class DesignContainer extends Component {
    state = {
        loading: true,
        removing: false,
    }
    
    componentDidMount() {
        const user = UserApi.GetInstance();
        user.designs.list({
            onSuccess: designs => {
                this.setState({ loading: false, designs, error: false })
            },
            onFailed: error => {
                this.setState({ loading: false, error })
            }
        })
    }

    handleClose = () => {
        this.setState({ loading: true, error: false })
        this.componentDidMount();
    }

    handleRemove = design => {
        const user = UserApi.GetInstance();
        user.designs.remove(design, {
            onSuccess: res => {
                this.setState({ removing: false, loading: true });
                this.componentDidMount()
            },
            onFailed: error => {
                this.setState({ removing: false, error })
            }
        })
        this.setState({ removing: true })
    }

    render() {
        const { classes } = this.props

        if (this.state.error) {
            return <ShortError error={this.state.error} onClose={this.handleClose} />
        }
        return (
            <div className={classes.root}>
                { this.state.loading ? <Loader classes={classes} /> : <DesignList designs={this.state.designs} onRemove={this.handleRemove} /> }
            </div>
        )
    }
}

export default withStyles(styles)(DesignContainer)
