import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog/Dialog';
import DialogTitle from 'material-ui/Dialog/DialogTitle';
import ListItem from 'material-ui/List/ListItem';
import Divider from 'material-ui/Divider/Divider';
import ListItemText from 'material-ui/List/ListItemText';
import DialogContent from 'material-ui/Dialog/DialogContent';
import withStyles from 'material-ui/styles/withStyles';
import FormControl from 'material-ui/Form/FormControl';
import Input from 'material-ui/Input/Input';
import InputLabel from 'material-ui/Input/InputLabel';
import DialogActions from 'material-ui/Dialog/DialogActions';
import Button from 'material-ui/Button/Button';

const styles = theme => ({

})

class DialogInput extends Component {
    constructor(props) {
        super(props)
        
        let value = props.value
        this.state = {
            value: value
        }
    }

    handleChange = ({ target }) => {
        this.setState({ value: target.value })
    }

    handleClick = () => {
        this.props.onValueChange(this.state.value)
    }

    render() {
        const { classes, type, name, label } = this.props
        
        const { value } = this.state
        return (
            <div className={classes.root}>
                <ListItem button onClick={this.props.onClick} className={classes.selected}>
                    <ListItemText primary={label} secondary={value} />
                </ListItem>
                <Divider />
                <Dialog open={this.props.open} onRequestClose={() => this.props.onValueChange(this.state.value)}>
                    <DialogTitle>{label}</DialogTitle>
                    <DialogContent>
                        <FormControl>
                            <InputLabel htmlFor={name}>{label}</InputLabel>
                            <Input type={type} name={name} onChange={this.handleChange} />
                        </FormControl>
                    </DialogContent>
                    <DialogActions>
                        <Button color={'primary'} onClick={this.handleClick}>
                            Done
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

export default withStyles(styles)(DialogInput);
