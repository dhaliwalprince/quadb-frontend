import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog/Dialog';
import DialogTitle from 'material-ui/Dialog/DialogTitle';
import ListItem from 'material-ui/List/ListItem';
import Divider from 'material-ui/Divider/Divider';
import ListItemText from 'material-ui/List/ListItemText';
import DialogContent from 'material-ui/Dialog/DialogContent';
import withStyles from 'material-ui/styles/withStyles';

const styles = theme => ({

})

class DialogSelect extends Component {
    constructor(props) {
        super(props)
        
        this.state = {
            selected: props.selected
        }
    }
    render() {
        const { classes, options, displayPropName, label } = this.props
        const { selected } = this.state
        return (
            <div className={classes.root}>
                <ListItem button className={classes.selected}>
                    <ListItemText primary={label} secondary={options[selected][displayPropName]} />
                </ListItem>
                <Divider />
                <Dialog open={this.props.open} onRequestClose={() => this.setState({ open: false })}>
                    <DialogTitle>Select an option</DialogTitle>
                    <DialogContent>
                        
                    </DialogContent>
                </Dialog>
            </div>
        )
    }
}

export default withStyles(styles)(DialogSelect);
