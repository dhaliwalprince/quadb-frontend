import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper/Paper';
import Typography from 'material-ui/Typography'
import FormControl from 'material-ui/Form/FormControl';
import InputLabel from 'material-ui/Input/InputLabel';
import Input from 'material-ui/Input';
import FormHelperText from 'material-ui/Form/FormHelperText';

const styles = theme => ({
    root: {
        padding: theme.spacing.unit
    }
})

class EditOrder extends Component {
    render() {
        const { classes } = this.props

        return (
            <Paper className={classes.root}>
                <Typography type='caption'>
                Edit the quantity:
                </Typography>
                <FormControl
                    disabled={this.state.disabled}
                    className={classes.formInput}
                    error={this.state.error}>
                    <InputLabel
                        htmlFor='Quantity'>
                        Quantity
                    </InputLabel>
                    <Input type={'number'} id={'quantity'} autoComplete="off" onChange={this.handleChange} />
                    <FormHelperText>{this.error && this.error.message}</FormHelperText>
                </FormControl>
            </Paper>
        )
    }
}

export default withStyles(styles)(EditOrder)
