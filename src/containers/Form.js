import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import FormControl from 'material-ui/Form/FormControl';
import Input, {InputLabel} from 'material-ui/Input'
import FormHelperText from 'material-ui/Form/FormHelperText';
import Button from 'material-ui/Button'

const styles = theme => ({
    form: {
        display: 'flex',
        flexDirection: 'column',
        margin: 'auto',
    },
    error: {
        fontSize: '0.75em',
        color: 'red'
    }
})

export function FormInput({ classes, column, onChange, disabled, error, value }) {
    if (typeof value !== 'undefined') {
        return (
            <FormControl
                disabled={disabled}
                className={classes.formInput}
                error={error}>
                <InputLabel
                    htmlFor={column.field}>
                    { column.name }
                </InputLabel>
                <Input type={column.type} id={column.field} autoComplete="off" onChange={onChange} value={value} />
                <FormHelperText>{error && error.message}</FormHelperText>
            </FormControl>
        )
    }
    return (
        <FormControl
            disabled={disabled}
            className={classes.formInput}
            error={error}>
            <InputLabel
                htmlFor={column.field}>
                { column.name }
            </InputLabel>
            <Input type={column.type} id={column.field} autoComplete="off" onChange={onChange} />
            <FormHelperText>{error && error.message}</FormHelperText>
        </FormControl>
    )
}

export function toType(type, value) {
    switch (type) {
        case 'number':
            return parseInt(value)
        case 'float':
            return parseFloat(value)
        default:
            return value
    }
}

class Form extends Component {
    state = {
        errors: [],
        globalError: null,
        values: [],
    }

    errorOf(column) {
        return this.state.errors.find(error => error.field === column.field)
    }

    componentWillReceiveProps(nextProps) {
        if (!this.props.globalError || !nextProps.globalError)
            return
        if (typeof this.props.globalError === 'object' &&
            typeof nextProps.globalError === 'object' &&
            nextProps.globalError.message !== this.props.globalError.message) {
            this.setState({ errors: [] })
        }
    }

    handleChange = (column, { target }) => {
        const { value } = target
        // does the value already exist in the state
        let mayBe = this.state.values.find(value => value.column.field === column.field)
        if (typeof mayBe === 'undefined') {
            const values = [ ...this.state.values ]
            values.push({ column, value })
            this.setState({values})
            return
        }

        const values = [ ...this.state.values ]
        mayBe.value = value

        this.setState({ values })
    }

    handleSubmit = event => {
        event.preventDefault();
        event.stopPropagation();
        const errors = []
        for (const column of this.props.columns) {
            let mayBe = this.state.values.find(value => value.column.field === column.field)
            if ((typeof mayBe === 'undefined' || !mayBe.length) && column.required) {
                errors.push({ field: column.field, message: `Invalid ${column.name}`})
                continue;
            }

            if (column.type !== 'number' && column.validation && column.validation.test(mayBe.value)) {
                continue;
            }

            if (column.type === 'number') {
                continue;
            }

            if (column.type === 'float' && column.validation && column.validation.test(mayBe.value)) {
                continue;
            }

            if (column.validation) {
                errors.push({ field: column.field, message: column.errorMessage })
                continue;
            }
        }

        this.setState(() => ({ errors }))

        const data = {}
        for (const value of this.state.values) {
            data[value.column.field] = toType(value.column.type, value.value)
        }
        this.props.onSubmit(data)
    }

    render() {
        const { columns, classes, disabled } = this.props

        return (
            <div className={classes.root}>
                <form
                    onSubmit={this.handleSubmit}
                    autoComplete="off"
                    className={classes.form}>
                    <div className={classes.error} dangerouslySetInnerHTML={{ __html: this.props.globalError && this.props.globalError.message }} />

                    {
                        columns.map((column, index) => {
                            return (
                                <FormInput column={column}
                                        key={index}
                                        classes={classes}
                                        disabled={disabled}
                                        error={this.errorOf(column)}
                                        onChange={this.handleChange.bind(this, column)} />
                            )
                        })
                    }
                    <FormControl className={classes.formControl}>
                        <Button color='primary' raised disabled={disabled} type="submit" className={classes.submit}>
                            {this.props.submitLabel ? this.props.submitLabel : 'Submit'}
                        </Button>
                    </FormControl>
                </form> 
            </div>
        )
    }
}

export default withStyles(styles)(Form);
