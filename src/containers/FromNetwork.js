import React, { Component } from 'react';

export class ApiCall extends Component {
    state = {
        loading: true,
        error: false,
    }

    makeRequest() {
        const params = this.props.request
        this.request = fetch(params.endpoint, params)
        this.request.then(res => res.json(),
                          error =>
                                 this.setState(() =>
                                        ({ error, loading: false })))
                    .then(json => {
                        if (json.status === 'FAILED') {
                            this.setState({ error: json, loading: false })
                            return;
                        }
                        this.setState({ error: false, loading: false, response: json })
                    })
                    .catch(error => this.setState({ error, loading: false }))
    }

    componentDidMount() {
        this.makeRequest();
    }

    render() {
        const { loading, error } = this.state;
        const { responsePropName, responseType } = this.props;

        let response = this.state.response;
        if (responseType === 'array' && typeof response === 'undefined') {
            response = []
        }
        return React.createElement(this.props.componentClass, {
            ...this.props.componentProps, loading, error, [responsePropName]: this.state.response
        })
    }
}

export default function FromNetwork(request, componentClass, { responsePropName, responseType }) {
    return props => <ApiCall
                        responsePropName={responsePropName}
                        componentProps={props}
                        responseType={responseType}
                       />
}
