import React, { Component } from 'react';
import { LinearProgress } from 'material-ui/Progress';
import UserApi from '../globals/user';

export default class Logout extends Component {
    componentDidMount() {
        const api = UserApi.GetInstance();
        api.logout()
        this.props.history.push('/')
    }

    render() {
        return (
            <div style={{}}>
                <LinearProgress />
            </div>
        )
    }
}
