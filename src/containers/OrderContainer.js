import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import Loader from '../components/Loader';
import OrderList from '../components/OrderList';
import Error from '../components/Error';
import UserApi from '../globals/user';
import auth from './auth';

const style = theme => ({
    orderList: {
        root: {

        }
    }
})

class OrderContainer extends Component {
    state = {
        loading: true,
    }

    componentDidMount() {
        const user = UserApi.GetInstance()
        user.orders.list({
            onSuccess: orders => {
                this.setState({ orders, loading: false, error: false })
            },
            onFailed: error => {
                this.setState({ error })
            }
        })
    }

    handleEdit = order => {
    }

    handleCancel = order => {

    }

    render() {
        const { classes } = this.props;
        if (this.state.error) {
            return <Error error={this.state.error} />
        }
        return (
            <div>
                {
                    this.state.loading ? <Loader classes={classes} /> : <OrderList orders={this.state.orders} onEdit={this.handleEdit} onCancel={this.handleCancel} />
                }
            </div>
        )
    }
}

export default auth(withStyles(style)(OrderContainer))
