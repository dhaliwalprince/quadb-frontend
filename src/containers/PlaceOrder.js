import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper'
import Typography from 'material-ui/Typography';
import OrderDetails from '../components/OrderDetails';
import Loader from '../components/Loader';
import Error from '../components/Error';
import UserApi from '../globals/user';
import DialogActions from 'material-ui/Dialog/DialogActions';
import Button from 'material-ui/Button/Button';
import AddressChooser from './AddressChooser';
import Divider from 'material-ui/Divider/Divider';
import CircularProgress from 'material-ui/Progress/CircularProgress';

const styles = theme => ({
    root: {
        padding: theme.spacing.unit * 2,
        maxWidth: 480,
        margin: 'auto'
    },
    divider: {
        marginTop: theme.spacing.unit * 2,
        marginBottom: theme.spacing.unit * 2
    }
})

class PlaceOrder extends Component {
    state = {
        loading: true,
        show: false,
        addressSelected: false,
    }

    componentDidMount() {
        const user = UserApi.GetInstance()
        user.orders.find(this.props.match.params.id, {
            onSuccess: order => {
                this.setState({ loading: false, order, error: false })
            },
            onFailed: error => this.setState({ error })
        })
    }

    handleClose = address => {
        if (typeof address === 'undefined') {
            return this.setState({ show: false });
        }

        this.setState({ show: false, addressSelected: true, address })
    }

    handlePlace = () => {
        const user = UserApi.GetInstance();
        user.orders.place(this.state.order, this.state.address.id, {
            onSuccess: () => {
                this.setState({ success: true })
            },
            onFailed: error => {
                this.setState({ error })
            }
        });
        this.setState({ placing: true })
    }

    render() {
        const {classes} = this.props;

        if (this.state.success) {
            return (
                <Paper className={classes.root}>
                    <Typography>Successfully placed order.</Typography>
                </Paper>
            )
        }

        if (this.state.error) {
            return <Error error={this.state.error} />
        }

        if (this.state.loading) {
            return <Loader classes={classes} />
        }

        const { order } = this.state
        return (
            <Paper className={classes.root}>
                <div className={classes.details}>
                        <Typography>
                            Order #: <b>{order.id}</b>
                        </Typography>
                        <Typography type={'caption'}>
                            Order details:
                        </Typography>
                        <OrderDetails details={order.order_details} order={order} />
                </div>
                <Divider className={classes.divider}/>
                <div className={classes.addressChooser}>
                    <Typography type={'caption'}>
                    Address
                    </Typography>
                    <Typography>{this.state.addressSelected ? this.state.address.toOneLine() : 'Choose an address.'}</Typography>
                    <AddressChooser onClose={this.handleClose} open={this.state.show} />
                    <Button raised onClick={() => this.setState({ show: true })}>Select</Button>
                </div>
                <DialogActions>
                    <Button color='primary' raised disabled={!this.state.addressSelected || this.state.placing} onClick={this.handlePlace}>
                        Continue
                    </Button>
                    { this.state.placing ? <CircularProgress size={24} /> : <div/> }
                </DialogActions>
            </Paper>
        )
    }
}

export default withStyles(styles)(PlaceOrder)
