import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import { Link } from 'react-router-dom';
import { CircularProgress } from 'material-ui/Progress'
import auth from './auth';
import UserApi from '../globals/user'
import Paper from 'material-ui/Paper'
import Error from '../components/Error';
import ProfilePage from '../components/ProfilePage'

const styles = theme => ({
    progressWrapper: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        minHeight: '480px'
    },
    progress: {
        display: 'block',
        margin: 'auto',
        verticalAlign: 'center'
    }
})

class ProfileLoader extends Component {
    state = {
        loading: true,
        user: null,
        error: null
    }

    componentDidMount() {
        const api = UserApi.GetInstance();
        this.request = api.Self({
            onSuccess: user => {
                this.setState({ loading: false, user })
            },
            onFailed: err => {
                this.setState({ loading: false, error: err })
            }
        })
    }

    render() {
        const { classes } = this.props
        if (this.state.loading) {
            return (
                <div classes={classes.progressWrapper}>
                    <CircularProgress className={classes.progress} />
                </div>
            )
        }

        if (this.state.error) {
            return (
                <div classes={classes.progressWrapper}>
                    <Error error={this.state.error} />
                </div>
            )
        }

        return (
            <ProfilePage user={this.state.user} />
        )
    }
}

export default auth(withStyles(styles)(ProfileLoader))
