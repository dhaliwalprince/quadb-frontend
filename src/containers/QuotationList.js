import React, { Component } from 'react';
import withStyles from 'material-ui/styles/withStyles';
import LoadErrComponent from '../components/LoadErrComponent';
import { AuthorizedApi } from '../globals/authorization';
import Typography from 'material-ui/Typography/Typography';
import ListItem from 'material-ui/List/ListItem';
import Link from 'react-router-dom/Link';
import Paper from 'material-ui/Paper/Paper';
import Button from 'material-ui/Button/Button';
import DialogActions from 'material-ui/Dialog/DialogActions';

const styles = theme => ({
    quote: {
        padding: theme.spacing.unit * 2,
        marginBottom: theme.spacing.unit * 2,
    }
})

class CQuotation extends Component {
    render() {
        const { classes, quote } = this.props;

        return (
            <Paper className={classes.quote}>
                <Typography className={classes.quoteHeading}>
                    Quotation #{quote.id}
                </Typography>
                <Typography>
                    Cloth Material: <b>{quote.cloth_material.name}</b> - {quote.cloth_material.description}
                </Typography>
                <Typography>
                    Category: <b>{quote.category.name}</b> - {quote.category.description}
                </Typography>
                <Typography>
                    Price: <b>Rs. {quote.price_per_item * quote.quantity}</b>
                </Typography>
                <Typography>
                    Price per quantity: <b>Rs. {quote.price_per_item}</b>
                </Typography>
                <DialogActions>
                <Button color={'primary'} component={Link} to={'/processOrder/confirm_quotation/'+quote.id}>
                    Place order
                </Button>
                </DialogActions>
            </Paper>
        )
    }
}

const Quotation = withStyles(styles)(CQuotation);

class QuotationListC extends Component {
    render() {
        const { classes } = this.props;
        return (
            <div className={classes.list}>
                { this.props.list.map((quotation, id) => {
                    return (
                        <Quotation key={id} quote={quotation} />
                    )
                })}
            </div>
        )
    }
}

const QuotationList = withStyles(styles)(QuotationListC);

export { QuotationList };

class QuotationListContainer extends Component {
    state = {
        loading: true,
        error: false,
        list: [],
    }

    componentDidMount() {
        AuthorizedApi.authApi.makeRequest(`/api/orders/requests/${this.props.match.params.requestId}/quotations`, {}, {
            onSuccess: list => {
                this.setState({ loading: false, error: false, list })
            },
            onFailed: error => {
                this.setState({ error, loading: false })
            }
        })
    }
    
    render() {
        const { classes } = this.props;
        return (
            <div className={classes.root}>
                <LoadErrComponent loading={this.state.loading} error={this.state.error}>
                    <QuotationList list={this.state.list} />
                </LoadErrComponent>
            </div>
        )
    }
}

export default withStyles(styles)(QuotationListContainer)
