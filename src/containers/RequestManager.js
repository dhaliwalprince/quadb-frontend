import React, { Component } from 'react';
import Paper from 'material-ui/Paper/Paper';
import LinearProgress from 'material-ui/Progress/LinearProgress';
import UserApi from '../globals/user';
import withStyles from 'material-ui/styles/withStyles';
import Error from '../components/Error';
import Typography from 'material-ui/Typography/Typography';
import { AuthorizedApi } from '../globals/authorization';
import DialogActions from 'material-ui/Dialog/DialogActions';
import Button from 'material-ui/Button/Button';
import SecureImage from '../components/SecureImage';
import { toList } from './Checkout';
import Link from 'react-router-dom/Link';

const styles = theme => ({
    root: {
        flex: 0.8,
        marginLeft: theme.spacing.unit * 2
    },
    paper: {
        padding: theme.spacing.unit * 2,
    },
    designPreview: {
        height: 200
    }
})

export function fromListToString(size_list) {
    return size_list.map(size => `${size.code} - ${size.quantity}`).join(', ')
}

function getReview(request) {
    switch (request.status) {
        case 'reviewing':
            return 'Under review'

        case 'accepted':
            return 'Accepted';

        case 'rejected':
            return 'Rejected';

        case 'cancelled':
            return 'Cancelled';
        default:
            return 'Unknown';
    }
}

class RequestManager extends Component {
    state = {
        loading: true,
    }

    componentDidMount() {
        const user = UserApi.GetInstance()
        user.orders.findRequestById(this.props.match.params.requestId, {
            onSuccess: request => {
                this.setState({ loading: false, error: false, request })

                const headers = new Headers()
                headers.set('Authorization', `Bearer ${AuthorizedApi.authApi.getToken()}`)
                fetch('/api/assets/images/private/'+this.state.request.design.image_url, { headers, redirect: 'manual'})
                    .then(res => {
                        if (!res.ok) {
                            return Promise.reject({ message: 'Unable to get image'})
                        }
                        let url = '';
                        if (res.headers.has('location')) {
                            url = res.headers.get('location')
                        }

                        this.setState({ imageUrl: url })
                    })
            },
            onFailed: error => {
                this.setState({ error, loading: false })
            }
        })
    }

    componentDidUpdate(oldProps) {
        if (oldProps.match.params.requestId === this.props.match.params.requestId)
            return
        this.componentDidMount()
    }

    shouldComponentUpdate(nextProps, nextState) {
        return (nextProps.match.params.designId !== this.props.match.params.designId
                || nextState.loading !== this.state.loading
                || nextState.imageUrl !== this.state.imageUrl
                || nextState.cancelling !== this.state.cancelling)
    }

    componentWillReceiveProps(props) {
        this.setState({ loading: true })
    }

    handleCancel = () => {
        const { request } = this.state
        const user = UserApi.GetInstance()
        user.orders.cancelRequest(request.id, {
            onSuccess: (res) => {
                this.setState({ cancelling: false, request: { ...request, status: 'cancelled' }})
            },
            onFailed: (error) => {
                this.setState({ error, cancelling: false })
            }
        })

        this.setState({ cancelling: true })
    }

    renderActions() {
        const { request } = this.state;

        const actions = [];
        if (request.status === 'reviewing') {
            actions.push({
                name: 'Cancel Request',
                handler: this.handleCancel,
            });
        }

        if (request.status === 'accepted') {
            actions.push({
                name: 'View Quotations',
                link: '/requests/'+request.id+'/quotations'
            })
        }

        return (
            <DialogActions>
                {
                    actions.map((action, index) => {
                        if (action.link) {
                            return (
                                <Button color="primary" raised component={Link} to={action.link} key={index}>
                                {action.name}
                                </Button>
                            )
                        }
                        return (
                            <Button color='primary' raised onClick={action.handler} key={index}>
                                {action.name}
                            </Button>
                        )
                    })
                }
            </DialogActions>
        )
    }
    
    render() {
        const { classes } = this.props
        
        return (
            <div className={classes.root}>
                <Paper className={classes.paper}>
                    { (this.state.loading || this.state.cancelling) && <LinearProgress /> }
                    { this.state.error && <Error error={this.state.error} />}
                    {this.state.request && (
                        <div className={classes.request}>
                            <Typography type='headline'>
                                Request #{ this.state.request.id }
                            </Typography>
                            <Typography>
                                Delivery Address: <b>{
    ((address) => {
        return `${address.line1}, ${address.line2}, ${address.city}, ${address.state}, ${address.pincode}, ${address.mobile}`
    })(this.state.request.address)}</b>
                            </Typography>
                            <Typography>
                                Quantity: <b>{ fromListToString(this.state.request.size_list)}</b>
                            </Typography>
                            <Typography>
                                Design: <b>{ this.state.request.design.name } ({ this.state.request.design.image_url.slice(0, -4)})</b>
                            </Typography>
                            <div className={classes.designImage}>
                                <SecureImage className={classes.designPreview} image_url={this.state.request.design.image_url} alt={this.state.request.design.name} />
                            </div>
                            <Typography>
                                Extra comments: { this.state.request.user_comments}
                            </Typography>
                            <br />
                            <Typography>
                                Current Status: <b>{getReview(this.state.request)}</b>
                            </Typography>
                            {
                                this.renderActions()
                            }
                        </div>
                    )}
                </Paper>
            </div>
        )
    }
}

export default withStyles(styles)(RequestManager);
