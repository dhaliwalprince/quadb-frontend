import React, { Component } from 'react';
import Paper from 'material-ui/Paper/Paper';
import withStyles from 'material-ui/styles/withStyles';
import Error from '../components/Error';
import LinearProgress from 'material-ui/Progress/LinearProgress';
import ListItem from 'material-ui/List/ListItem';
import ListItemText from 'material-ui/List/ListItemText';
import UserApi from '../globals/user'
import Typography from 'material-ui/Typography/Typography';

const styles = theme => ({
    padding: {
        padding: theme.spacing.unit * 2,
    },
    root: {
        flex: 0.2,
        minWidth: 300,
        maxWidth: 500,
    }
})

class RequestI extends  Component {
    render() {
        const { request, onClick } = this.props;
        return (
            <ListItem button onClick={() => onClick(request)}>
                <ListItemText primary={`Request #${request.id}`} secondary={`Quantity: ${request.quantity} ⚫ Design: ${request.design.name}`} />
            </ListItem>
        )
    }
}

const Request = withStyles(styles)(RequestI);
export { Request };

class RequestsContainer extends Component {
    state = {
        loading: true,
        error: false,
        requests: []
    }

    componentDidMount() {
        const user = UserApi.GetInstance();
        user.orders.listRequests({
            onSuccess: requests => {
                this.setState({ requests, error: false, loading: false })
            },
            onFailed: error => {
                this.setState({ error, loading: false })
            }
        })
    }

    handleClick = request => {
        this.props.history.push('/manage/requests/'+request.id)
    }

    render() {
        const { classes } = this.props

        return (
            <div className={classes.root}>
                <Paper className={classes.paper}>
                    <Typography type='headline' className={classes.padding}>
                        Manage your requests
                    </Typography>
                    { this.state.loading && <LinearProgress /> }
                    { this.state.error && <Error error={this.state.error} />} 
                    {
                        this.state.requests.map((request, id) => {
                            return (
                                <Request onClick={this.handleClick} request={request} key={id} />
                            )
                        })
                    }
                </Paper>
            </div>
        )
    }
}

export default withStyles(styles)(RequestsContainer);
