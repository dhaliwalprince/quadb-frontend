import React, { Component } from 'react';
import withStyles from 'material-ui/styles/withStyles';
import Dialog from 'material-ui/Dialog/Dialog';
import DialogTitle from 'material-ui/Dialog/DialogTitle';
import DialogContent from 'material-ui/Dialog/DialogContent';
import SizeSpecInput from './SizeSpecInput';

const styles = theme => ({

})

class SizeChooserDialog extends Component {
    render() {
        const { classes, open } = this.props;

        return (
            <Dialog open={open}>
                <DialogTitle>
                    Specify the quantity for each size category.
                </DialogTitle>
                <DialogContent>
                    <SizeSpecInput onChange={this.props.onChange} />
                </DialogContent>
            </Dialog>
        )
    }
}

export default withStyles(styles)(SizeChooserDialog);
