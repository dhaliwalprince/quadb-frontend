import React, { Component } from 'react';
import withStyles from 'material-ui/styles/withStyles';
import { FormInput } from './Form';
import DialogActions from 'material-ui/Dialog/DialogActions';
import Button from 'material-ui/Button/Button';

const styles = theme => ({

})

const sizes = [
    'XXS',
    'XS',
    'S',
    'M',
    'L',
    'XL',
    'XXL'
]

class SizeSpecInput extends Component {
    constructor(props) {
        super(props)

        let values = {}
        sizes.forEach(code => values[code] = 0)
        this.state = {
            values,
        }
    }

    handleChange(code) {
        return ({ target }) => {
            this.setState({ values: { ...this.state.values, [code]: parseInt(target.value) }})
        }
    }

    getValues() {
        return this.state.values;
    }

    render() {
        const { classes, onChange } = this.props;

        return (
            <div className={classes.root}>
                {
                    sizes.map((code, id) => {
                        const column = {
                            name: code,
                            field: code,
                            type: 'number'
                        }
                        return (
                            <div className={classes.formInput}>
                                <FormInput column={column} classes={classes} value={this.state.values[code]} type="number" onChange={this.handleChange(code)} />
                            </div>
                        )
                    })
                }
                <DialogActions>
                    <Button primary raised onClick={() => onChange(this.state.values)}>
                    Save
                    </Button>
                </DialogActions>
            </div>
        )
    }
}

export default withStyles(styles)(SizeSpecInput);
