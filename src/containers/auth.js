import React, { Component } from 'react';
import LoginOrSignupOption from '../components/LoginOrSignupOption';
import UserApi from '../globals/user'

export default function auth(component) {
    return props => {
        const api = UserApi.GetInstance()

        if (api.authApi.loggedIn) {
            return React.createElement(component, props)
        } else {
            return <LoginOrSignupOption />
        }
    }
}
