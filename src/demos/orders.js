const orders = [
    {
        user_id: 1,
        order_details: {
            design: {
                name: "Design by me.",
                user_id: 1,
                image_url: "http://quadb.in/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/p/o/polo_5.png",
                is_empty: false
            },
            cloth_material: {
                name: "Premium Cotton",
                features: `180-200 GSM, best sweat absorbent,
                ideal for sensitive skin, pre-shrunk, soft
                washed`,
                price: 300,
            },
            category: {
                name: 'Polo T-Shirt',
                features: ``,
                price: 200,
            },
            info: {
                item_price: 500,
                quantity: 15,
                order_id: 1
            }
        },
        events: [
            {
                name: "Placed",
                event_type: "ORDER_PLACED",
                completed: true,
                time: new Date(new Date().getTime() - 24 * 60 * 60 * 1000)
            },
            {
                name: "Shipping",
                event_type: "ORDER_SHIPPING",
                completed: false,
                time: new Date()
            },
            {
                name: "Delivered",
                event_type: "ORDER_DELIVERED",
                completed: false,
                time: new Date()
            }
        ]
    }
]

const cart = [
    {
        order_details: {
            design: {
                name: "Design by me.",
                user_id: 1,
                image_url: "http://quadb.in/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/p/o/polo_5.png",
                is_empty: false
            },
            cloth_material: {
                name: "Premium Cotton",
                features: `180-200 GSM, best sweat absorbent,
                ideal for sensitive skin, pre-shrunk, soft
                washed`,
                price: 300,
            },
            category: {
                name: 'Polo T-Shirt',
                features: ``,
                price: 200,
            },
            info: {
                item_price: 500,
                quantity: 15,
                order_id: 1
            }
        },
    },
    {
        order_details: {
            design: {
                name: "Design by me.",
                user_id: 1,
                image_url: "http://quadb.in/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/p/o/polo_5.png",
                is_empty: false
            },
            cloth_material: {
                name: "Premium Cotton",
                features: `180-200 GSM, best sweat absorbent,
                ideal for sensitive skin, pre-shrunk, soft
                washed`,
                price: 300,
            },
            category: {
                name: 'Polo T-Shirt',
                features: ``,
                price: 200,
            },
            info: {
                item_price: 500,
                quantity: 15,
                order_id: 1
            }
        },
    }
]

export { cart };

export default orders;
