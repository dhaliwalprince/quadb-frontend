import AdminApi from '../admin';

export default class order_manager {
    constructor(options) {
        this.options = options;
    }

    list(options) {
        let filter = '';
        if (typeof options.filter !== 'undefined') {
            filter = options.filter;
        }
        AdminApi.authApi.makeRequest('/api/orders?filter=' + filter, {}, options)
    }

    acceptOrder(order, options) {
        AdminApi.authApi.makeRequest(`/api/users/${order.userId}/orders/${order.id}/accept`, {}, options);
    }

    rejectOrder(order, options) {
        AdminApi.authApi.makeRequest(`/api/users/${order.userId}/orders/${order.id}/reject`, {}, options);
    }

    markDelivered(order, options) {
        AdminApi.authApi.makeRequest(`/api/users/${order.userId}/orders/${order.id}/mark_delivered`, {}, options);
    }
}
