import { AuthorizedApi } from './authorization';

const AdminApi = AuthorizedApi;

export default AdminApi;
