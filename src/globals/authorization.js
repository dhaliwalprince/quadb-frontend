import {AUTH_TOKEN_URL, NEW_USER_URL } from './urls'

export class AuthorizedApi {
    token = null
    loggedIn = false

    static authApi = new AuthorizedApi()
    constructor() {
        if (window.localStorage) {
            this.token = localStorage.getItem('token')
            if (this.token) {
                this.loggedIn = true
                return
            }

            this.loggedIn = false
        }
    }

    login(username, password, options) {
        const auth = btoa(`${username}:${password}`)
        const headers = new Headers()
        headers.set('Authorization', `Basic ${auth}`)

        const config = {
            method: 'GET',
            credentials: 'include',
            headers,
        }
        this.requestPromise = fetch(AUTH_TOKEN_URL, config)

        this.requestPromise.catch(reason => {
            options.onFailed(reason)
        });

        this.requestPromise.then(res => res.json())
            .then(res => {
                if (res.status && res.status === 'FAILED') {
                    options.onFailed(res)
                } else {
                    this.token = res.token;
                    this.loggedIn = true
                    localStorage.setItem('token', this.token)
                    options.onSuccess()
                }
            })
            .catch(reason => {
                options.onFailed(reason)
            })
    }

    signup(name, email, password, options) {
        const body = { name, email, password }
        const headers = new Headers()
        headers.set('Content-Type', 'application/json')
        const config = {
            method: 'POST',
            body: JSON.stringify(body),
            headers
        }
        const promise = fetch(NEW_USER_URL, config)

        promise.then(res => res.json())
            .then(res => {
                if (res.status && res.status === 'FAILED') {
                    options.onFailed(res)
                } else {
                    options.onSuccess(res)
                }
            })
            .catch(err => options.onFailed(err))

        return promise
    }

    cancelLogin() {
        if (this.requestPromise) {
            this.requestPromise.cancel()
        }
    }

    prepareRequest() {
        return this.token !== null && this.loggedIn
    }


    getToken() {
        return this.token
    }

    makeRequest(url, config, options) {
        if (!this.prepareRequest()) {
            options.onFailed({ message: 'User not logged in.' })
            return
        }

        const headers = new Headers();
        headers.set('Authorization', `Bearer ${this.token}`)
        config.headers = headers
        fetch(url, config)
            .then(res => res.json())
            .then(res => {
                if (res.status && res.status === 'FAILED') {
                    options.onFailed(res);
                } else {
                    options.onSuccess(res);
                }
            })
            .catch(options.onFailed)
    }

    metaRequest(endpoint, method, body) {
        const headers = new Headers();
        headers.set('Authorization', `Bearer ${this.token}`)
        return {
            endpoint,
            method,
            body,
            headers, 
            types: [
                {
                    type: 'REQUEST'
                },
                'RECEIVE',
                'FAILURE'
            ]
        }
    }

    logout() {
        this.loggedIn = false
        if (window.localStorage) {
            window.localStorage.removeItem('token')
        }

        this.token = null
        this.loggedInUser = null
    }
}

