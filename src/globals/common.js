import { CATEGORIES_URL, MATERIALS_URL, PRINTING_URL } from "./urls";

let api = {
    cats: null,
    clos: null,
    pris: null,
    categories(options) {
        if (this.cats) {
            return setTimeout(() => options.onSuccess(this.cats))
        }
        fetch(CATEGORIES_URL)
            .then(res => res.json())
            .then(categories => options.onSuccess(this.cats = categories))
            .catch(options.onFailed)
    },
    clothes(options) {
        if (this.clos) {
            return setTimeout(() => options.onSuccess(this.clos))
        }
        fetch(MATERIALS_URL)
            .then(res => res.json())
            .then(clothes => options.onSuccess(this.clos = clothes))
            .catch(options.onFailed)
    },
    prints(options) {
        if (this.pris) {
            return setTimeout(() => options.onSuccess(this.pris))
        }

        fetch(PRINTING_URL)
            .then(res => res.json())
            .then(prints => options.onSuccess(this.pris = prints))
            .catch(options.onFailed)
    }
}

export default api;