import { ORDERS_URL, CART_URL, REQUEST_URL } from './urls'
import { AuthorizedApi } from './authorization';

export const eventTypes = {
    ORDER_REQUESTED: '5:requested',
    ORDER_PLACED: '30:created',
    ORDER_ACCEPTED: '40:accepted',
    ORDER_DELIVERED: '100:delivered',
    ORDER_CANCELLED: '200:cancelled',
    ORDER_REJECTED: '500:rejected'
}

const utils = {
    check:      (events, type) => {
        let event = events.find(event => event.type === type);
        return typeof event !== 'undefined'
    },

    isPlaced:    order => {
        return utils.check(order.events, eventTypes.ORDER_PLACED)
    },

    isDelivered: order => {
        return utils.check(order.events, eventTypes.ORDER_DELIVERED)
    },

    isAccepted:  order => {
        return utils.check(order.events, eventTypes.ORDER_ACCEPTED)
    },

    isOld: order => {
        return utils.check(order.events, eventTypes.ORDER_DELIVERED) ||
               utils.check(order.events, eventTypes.ORDER_CANCELLED) ||
               utils.check(order.events, eventTypes.ORDER_REJECTED)
    }
}

export default class Orders {
    static utils = utils

    /// add the order to the cart
    addToCart(order, options) {
        AuthorizedApi.authApi.makeRequest(CART_URL, { method: 'POST', body: JSON.stringify(order) }, options)
    }

    createRequest(order, options) {
        AuthorizedApi.authApi.makeRequest(REQUEST_URL, { method: 'POST', body: JSON.stringify(order) }, options)
    }

    listRequests(options) {
        AuthorizedApi.authApi.makeRequest(REQUEST_URL+'s', { method: 'GET' }, options)
    }

    findRequestById(id, options) {
        AuthorizedApi.authApi.makeRequest(REQUEST_URL+'s/'+id, {}, options)
    }

    cancelRequest(id, options) {
        AuthorizedApi.authApi.makeRequest(REQUEST_URL+'s/'+id+'/cancel', {}, options)
    }

    // will list all the orders currently placed of the user
    list(config) {
        AuthorizedApi.authApi.makeRequest(ORDERS_URL, {}, {
            onSuccess: orders => {
                const placed = orders.filter(utils.isPlaced)
                config.onSuccess(placed)
            },
            onFailed: config.onFailed
        })
    }

    // list old orders
    listOld(options) {
        AuthorizedApi.authApi.makeRequest(ORDERS_URL, {}, {
            onSuccess: orders => {

            }
        })
    }

    find(orderId, options) {
        AuthorizedApi.authApi.makeRequest(ORDERS_URL + '/' + orderId, {}, options)
    }
    // places order
    place(order, addressId, options) {
        AuthorizedApi.authApi.makeRequest(ORDERS_URL + '/' + order.id + '/place?addressId=' + addressId, {}, options)
    }

    // updates the order
    update(order, options) {
        const config = {
            method: 'POST'
        }
        AuthorizedApi.authApi.makeRequest(ORDERS_URL + '/' + order.id + '/info', config, options)
    }

    // cancels the order
    cancel(order, options) {
        AuthorizedApi.authApi.makeRequest(ORDERS_URL + '/' + order.id + '/cancel', {}, options)
    }

    // remove item from cart
    remove(order, options) {
        AuthorizedApi.authApi.makeRequest(ORDERS_URL + '/' + order.id + '/remove', { method: 'DELETE' }, options)
    }

    // list the orders in the cart
    cart(options) {
        AuthorizedApi.authApi.makeRequest(ORDERS_URL, {}, {
            onSuccess: orders => {
                const cart = orders.filter(order => order.events.length === 1)
                options.onSuccess(cart)
            },
            onFailed: options.onFailed
        })
    }
}
