import { USER_URL, ADDRESS_URL, DESIGNS_URL } from './urls'
import { AuthorizedApi } from './authorization';
import Orders from './order';

class User {
    id = ''
    name = ''
    email = ''
    addresses = null

    constructor(id, name, email) {
        this.id = id 
        this.name = name
        this.email = email
        this.addresses = new AddressListProxy(this)
    }
}

class DesignStore {
    save(design, options) {
        AuthorizedApi.authApi.makeRequest(DESIGNS_URL, {
            method: 'POST',
            body: JSON.stringify(design),
        }, options)
    }

    remove(design, options) {
        AuthorizedApi.authApi.makeRequest(DESIGNS_URL, {
            method: 'DELETE',
            body: JSON.stringify(design)
        }, options)
    }

    list(options) {
        AuthorizedApi.authApi.makeRequest(DESIGNS_URL, {
            method: 'GET'
        }, options)
    }

    findByHash(hash, options) {
        AuthorizedApi.authApi.makeRequest(DESIGNS_URL+'/h/'+hash, {}, options)
    }
}

class AddressListProxy {
    dirty = true
    addressList = []

    constructor(user) {
        this.user = user;
    }

    all(options) {
        if (!this.dirty) {
            options.onSuccess(this.addressList)
            return;
        }
        const config = {
            method: 'GET'
        }
        AuthorizedApi.authApi.makeRequest(ADDRESS_URL, config, {
            onSuccess: addresses => {
                this.addressList = addresses
                this.dirty = false
                
                this.addressList.map(address => 
                    address.toOneLine = function() {
                        return `${this.line1}${this.line2 && ', ' + this.line2}, ${this.city}, ${this.state},
                                ${this.pincode}, Mobile: ${this.mobile}`
                    }
                );
                    
                options.onSuccess(this.addressList)
            },
            onFailed: options.onFailed
        })
    }

    save(address, options) {
        const config = {
            method: 'POST',
            body: JSON.stringify(address)
        }

        AuthorizedApi.authApi.makeRequest(ADDRESS_URL, config, {
            onSuccess: () => {
                this.dirty = true
                options.onSuccess(this.addressList)
            },
            onFailed: options.onFailed
        })
    }

    delete(address, options) {
        delete address.toOneLine
        const config = {
            method: 'DELETE',
            body: JSON.stringify(address)
        }

        AuthorizedApi.authApi.makeRequest(ADDRESS_URL, config, {
            onSuccess: () => {
                this.dirty = true
                options.onSuccess(this.addressList)
            },
            onFailed: options.onFailed
        })
    }
}

class UserApi {
    loggedInUser = null
    addresses = new AddressListProxy(null)
    orders = new Orders()
    designs = new DesignStore()
    authApi = AuthorizedApi.authApi

    login(...args) {
        AuthorizedApi.authApi.login(...args)
    }

    signup(...args) {
        AuthorizedApi.authApi.signup(...args)
    }

    logout(...args) {
        AuthorizedApi.authApi.logout(...args)
    }

    // returns the logged in user
    Self(options) {
        if (this.loggedIn && this.loggedInUser) {
            options.onSuccess(this.loggedInUser)
            return true;
        }

        if (!AuthorizedApi.authApi.prepareRequest()) {
            return false
        }

        const headers = new Headers()
        headers.set('Authorization', `Bearer ${AuthorizedApi.authApi.token}`)
        const config = {
            method: 'GET',
            headers
        }

        const promise = fetch(USER_URL, config)
        promise.then(res => res.json())
                .then(res => {
                    if (res.status && res.status === 'FAILED') {
                        this.logout()
                        options.onFailed(res)
                    } else {
                        this.loggedInUser = new User(res.id, res.name, res.email)
                        AuthorizedApi.authApi.loggedIn = true
                        options.onSuccess(this.loggedInUser)
                    }
                })
                .catch(err => options.onFailed(err))

        return promise
    }

    static userApi = new UserApi()

    static GetInstance() {
        return UserApi.userApi
    }
}


export default UserApi;
