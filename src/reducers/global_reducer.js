import { ADD_ALERT, REMOVE_ALERT, ADD_WORK, REMOVE_WORK } from "../actions/globals";
import { combineReducers } from 'redux'

function alerts(state = [], action) {
    switch (action.type) {
        case ADD_ALERT:
            return [ ...state, action.alert ]

        case REMOVE_ALERT:
            let nextState = [ ...state ]
            nextState = [ ...nextState.slice(0, action.alertId), ...nextState.slice(action.alertId + 1)]
            return nextState;

        default:
            return state
    }
}

function works(state = [], action) {
    switch (action.type) {
        case ADD_WORK:
            return [ ...state, action.work ]

        case REMOVE_WORK:
            return [ ...state.slice(0, action.workId), ...state.slice(action.workId+1) ]

        default:
            return state
    }
}

export const globals = combineReducers({
    alerts,
    works,
})