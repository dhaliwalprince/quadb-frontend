class RouteMap {
    constructor() {
        this.routeMap = {};
        this.routes = [];
    }

    AddRoute(name, route) {
        this.routeMap[name] = route;
        this.routes.push(route)
    }

    GetRoute(name) {
        return this.routeMap[name]
    }

    GetRoutes() {
        return this.routes;
    }
}

if (typeof window._routeMap === 'undefined') {
    window._routeMap = new RouteMap()
}

const routeMap = window._routeMap;

export default routeMap;
