/**
 * @class Schema
 * @description schema provides a basic utility class for creating schemas
 */
export default class Schema {
    raw = {}

    constructor(raw) {
        this.raw = new Proxy(raw, {
            get: this.get,
        });
    }

    get = function(target, name) {
        return name in target ? target[name] : this.raw[name]
    }
}
