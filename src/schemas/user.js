import Schema from "./schema";

const userSchema = {
    name: 'User',
    headline: 'Users',
    columns: [
        {
            name: 'ID',
            key: 'id',
            type: 'number',
            editable: false,
            creatable: false,
            resizable: true,
            required: false,
            show: true,
            width: 100,
        },
        {
            name: 'Name',
            key: 'name',
            type: 'text',
            editable: true,
            creatable: true,
            resizable: true,
            required: true,
            show: true,
        },
        {
            name: 'Email',
            key: 'email',
            type: 'text',
            editable: true,
            resizable: true,
            creatable: true,
            required: true,
            show: true,
        }
    ],

    related: [
        {
            name: 'Address',
            route: 'user_address'
        }
    ],
    urls: {
        'GET': '/api/users',
        'DELETE': '/api/users/[id]',
        'POST': '/api/users'
    }
}

const user = new Schema(userSchema);
export default userSchema;
