import React, { Component } from 'react';
import Paper from 'material-ui/Paper/Paper';
import Typography from 'material-ui/Typography/Typography';
import DesignPreview from '../components/DesignPreview';
import withStyles from 'material-ui/styles/withStyles';
import DialogActions from 'material-ui/Dialog/DialogActions';
import Button from 'material-ui/Button/Button';
import { fromListToString } from '../containers/RequestManager';
import { Link } from 'react-router-dom';

const styles = theme => ({
    root: {
        padding: theme.spacing.unit * 2,
        flex: 8
    }
})

class AdminRequestContainer extends Component {
    render() {
        const { classes, request, onAccept, onReject, onReview, onQuote } = this.props;

        return (
            <Paper className={classes.root}>
                <Typography type="headline">
                    Request #{request.id}
                </Typography>
                <Typography>
                    <b>User</b>: { request.user_id}
                </Typography>
                <div className={classes.designPreview}>
                    <Typography>
                        <b>Design</b>: #{request.design.id}({request.design.name})
                    </Typography>
                    <DesignPreview design={request.design} />
                </div>
                <Typography>
                    <b>Quantity</b>: { fromListToString(request.size_list)}
                </Typography>
                <Typography>
                    <b>Address</b>:
                    {
                        ((address) => {
                            return `${address.line1}, ${address.line2}, ${address.city}, ${address.state}, ${address.pincode}, ${address.mobile}`
                        })(request.address)
                    }
                </Typography>
                <DialogActions>
                    <Button raised onClick={() => onAccept(request)} disabled={request.status === 'cancelled'}>
                    Accept
                    </Button>
                    <Button raised onClick={() => onReject(request)} disabled={request.status === 'rejected'}>
                    Reject
                    </Button>
                    <Button raised onClick={() => onReview(request)}>
                    Set Review Status
                    </Button>
                    <Button raised component={Link} to={"/manage/quotations?requestId="+request.id}>
                    Generate quotations
                    </Button>
                </DialogActions>
            </Paper>
        )
    }
}

export default withStyles(styles)(AdminRequestContainer);
