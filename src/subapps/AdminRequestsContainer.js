import React, { Component, Fragment } from 'react';
import withStyles from 'material-ui/styles/withStyles';
import LinearProgress from 'material-ui/Progress/LinearProgress';
import ListItem from 'material-ui/List/ListItem';
import ListItemText from 'material-ui/List/ListItemText';
import { AuthorizedApi } from '../globals/authorization';
import Error from '../components/Error';
import AdminRequestContainer from './AdminRequestContainer';
import Typography from 'material-ui/Typography/Typography';
import Dialog from 'material-ui/Dialog/Dialog';
import DialogTitle from 'material-ui/Dialog/DialogTitle';
import DialogContent from 'material-ui/Dialog/DialogContent';
import DialogContentText from 'material-ui/Dialog/DialogContentText';
import DialogActions from 'material-ui/Dialog/DialogActions';
import Button from 'material-ui/Button/Button';
import withRouter from 'react-router-dom/withRouter';

const styles = theme => ({
    requestList: {
        maxWidth: '400px',
        flex: 4,
    },
    grid: {
        display: 'flex',
        flexDirection: 'row',
    },
    pad: {
        paddingLeft: theme.spacing.unit * 2,
    }
})

export function totalQuantity(sizes) {
    let q = 0;
    for (var size of sizes) {
        q += size.quantity
    }

    return q;
}

const RequestShort = withStyles(styles)(({ classes, request, onSelect }) => {
    return (
        <ListItem button onClick={onSelect}>
            <ListItemText primary={`Request #${request.id}`} secondary={
                `User: ${request.user_id} | Quantity: ${totalQuantity(request.size_list)} | Status: ${request.status}`
            } />
        </ListItem>
    )
})

const RequestList = withStyles(styles)(({ classes, requests, onSelect }) => {
    return (
        <div className={classes.requestList}>
            <Typography className={classes.pad}>
                <b>Requests</b>: {requests.length}
            </Typography>
            {
                requests.map((request, id) => {
                    return (
                        <RequestShort request={request} key={id} onSelect={() => onSelect(request)} />
                    )
                })
            }
        </div>
    )
})

class AdminRequestsContainer extends Component {
    state = {
        loading: true,
        requests: [],
        filter: 'reviewing',
        open: false,
        actions: []
    }

    _componentDidMount() {
        AuthorizedApi.authApi.makeRequest('/api/orders/requests?filter='+this.state.filter, {}, {
            onSuccess: requests => {
                this.setState({ loading: false, error: false, requests });
            },
            onFailed: error => {
                this.setState({ error, loading: false })
            }
        })
    }

    componentDidMount() {
        this._componentDidMount();
    }

    handleSelect = request => {
        this.setState({ selected: request })
    }

    handleFilter = filter => () => {
        if (this.state.filter !== filter) {
            this.setState({ filter: filter, loading: true, error: false }, () => {
                this._componentDidMount()
            })
        }
        
        return false;
    }

    handleAction = action => request => {
        this.setState({ actions: [ ...this.state.actions, { type: action, request, message: `Perform '${action}' action on request: ${request.id}`}], open: true})
    }

    handleConfirm = () => {
        const action = this.state.actions[0];
        this.setState({ loading: true, open: false, actions: []})

        AuthorizedApi.authApi.makeRequest('/api/orders/requests/'+action.request.id+'/setStatus?status='+action.type, {}, {
            onSuccess: () => {
                if (action.type === 'accepted') {
                    this.props.history.push('/manage/quotations?requestId='+action.request.id)
                }
                this.setState({ loading: false, error: false })
            },
            onFailed: error => {
                this.setState({ loading: false, error })
            }
        })
    }

    handleCancel = () => {
        this.setState({open: false})
    }

    handleGenQuote = request => {
    }

    render() {
        const { classes } = this.props;

        const filters = [
            'reviewing',
            'accepted',
            'rejected',
            'cancelled'
        ]
    
        return (
            <div className={classes.root}>
                <Typography className={classes.pad}>Showing requests with '{this.state.filter}' status. Change filter to:
                    {
                        filters.map((filter, id) => {
                            if (this.state.filter === filter) {
                                return <span key={id}>&nbsp;{filter}&nbsp;</span>
                            }
                            return (
                                <Fragment key={id}>&nbsp;<a href='#' key={id} onClick={this.handleFilter(filter)}><b>{filter}</b></a>&nbsp;</Fragment>
                            )
                        })
                    }
                </Typography>
                {
                    this.state.loading && <LinearProgress />
                }
                {
                    this.state.error && <Error error={this.state.error} />
                }
                <div className={classes.grid}>
                    <RequestList requests={this.state.requests} onSelect={this.handleSelect} />
                    { this.state.selected && <AdminRequestContainer request={this.state.selected} onQuote={this.handleGenQuote} onAccept={this.handleAction('accepted')} onReject={this.handleAction('rejected')} onReview={this.handleAction('reviewing')} /> }
                </div>

                <Dialog open={this.state.open}>
                    <DialogTitle>Please confirm</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Following action(s) will be performed:
                            {
                                this.state.actions.map((action, id) => (
                                    <Typography key={id}>
                                         - {action.message}
                                    </Typography>
                                ))
                            }
                        </DialogContentText>
                        <DialogActions>
                            <Button onClick={this.handleCancel}>
                               Cancel
                            </Button>
                            <Button onClick={this.handleConfirm}>
                               Confirm
                            </Button>
                        </DialogActions>
                    </DialogContent>
                </Dialog>
            </div>
        )
    }
}

export default withStyles(styles)(AdminRequestsContainer);
