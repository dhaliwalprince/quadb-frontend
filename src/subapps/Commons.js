import React from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { globals } from '../reducers/global_reducer';
import AlertContainer from '../containers/AlertContainer';

let store = createStore(globals)

const Commons = () => (
    <Provider store={store}>
        <AlertContainer />
    </Provider>
)

export default Commons;
