package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"time"

	"github.com/princedhaliwal/quadb/go/context"
	"github.com/princedhaliwal/quadb/go/models"
)

func main() {
	_, err := os.Stat("TIMESTAMP")
	if err == nil {
		fmt.Println("Delete TIMESTAMP if you want to force install the data")
		return
	}
	home := os.Getenv("USER")
	f, err := os.Open(path.Join("/home", home, "go/src/github.com/princedhaliwal/quadb/config/material.json"))
	if err != nil {
		panic(err)
	}
	decoder := json.NewDecoder(f)

	materials := []models.ClothMaterial{}
	err = decoder.Decode(&materials)
	if err != nil {
		panic(err)
	}

	db := context.Config.GormDb

	models.InitializeModels(db)
	for _, material := range materials {
		err = db.Save(&material).Error
		if err != nil {
			fmt.Errorf("Unable to install material: %v: %v", material, err)
		}
	}

	err = ioutil.WriteFile("TIMESTAMP", []byte(time.Now().String()), os.ModeCharDevice)
	if err != nil {
		panic(err)
	}

	fmt.Println("Successfully saved timestamp")
}
