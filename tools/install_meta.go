package main

import (
	"log"
	"reflect"
	"strings"
	"unicode"
	"unicode/utf8"

	"github.com/princedhaliwal/quadb/go/context"
	"github.com/princedhaliwal/quadb/go/models"
)

type buffer struct {
	r         []byte
	runeBytes [utf8.UTFMax]byte
}

func (b *buffer) write(r rune) {
	if r < utf8.RuneSelf {
		b.r = append(b.r, byte(r))
		return
	}
	n := utf8.EncodeRune(b.runeBytes[0:], r)
	b.r = append(b.r, b.runeBytes[0:n]...)
}

func (b *buffer) indent() {
	if len(b.r) > 0 {
		b.r = append(b.r, '_')
	}
}

func underscore(s string) string {
	b := buffer{
		r: make([]byte, 0, len(s)),
	}
	var m rune
	var w bool
	for _, ch := range s {
		if unicode.IsUpper(ch) {
			if m != 0 {
				if !w {
					b.indent()
					w = true
				}
				b.write(m)
			}
			m = unicode.ToLower(ch)
		} else {
			if m != 0 {
				b.indent()
				b.write(m)
				m = 0
				w = false
			}
			b.write(ch)
		}
	}
	if m != 0 {
		if !w {
			b.indent()
		}
		b.write(m)
	}
	return string(b.r)
}

func getType(t reflect.Type) string {
	switch t.Kind() {
	case reflect.Int:
	case reflect.Uint:
		return "number"

	case reflect.String:
		return "text"

	case reflect.Bool:
		return "boolean"

	default:
		return "text"
	}
	return "text"
}

func installModel(model interface{}) {
	db := context.Config.GormDb

	value := reflect.ValueOf(model)

	meta := models.Meta{}
	meta.Name = underscore(value.Type().Name())
	meta.Class = underscore(value.Type().PkgPath() + "." + value.Type().Name())
	meta.Headline = value.Type().Name()
	err := db.Save(&meta).Error
	if err != nil {
		log.Panicf("Unable to save %s due to error: %v", meta.Name, err)
	}

	for i := 0; i < value.Type().NumField(); i++ {
		field := value.Type().Field(i)

		if field.Type.Kind() == reflect.Struct || field.Type.Kind() == reflect.Array {
			continue
		}

		editable := field.Type.Kind() == reflect.Uint && field.Type.Name() == "ID"

		t := getType(field.Type)
		column := models.Column{
			MetaId:       meta.ID,
			Name:         field.Name,
			Key:          underscore(field.Name),
			Editable:     editable,
			Creatable:    editable,
			Required:     editable,
			Type:         t,
			IsPrimaryKey: field.Type.Name() == "ID",
			IsForeignKey: strings.HasSuffix(field.Type.Name(), "Id"),
		}
		log.Printf("Saving column(%s) for %s", column.Name, meta.Name)
		err = db.Save(&column).Error
		if err != nil {
			log.Panicf("Unable to save column due to error: %v", err)
		}
	}
}

func main() {
	context.Config.GormDb.SingularTable(true)

	mdls := []interface{}{models.Meta{}, models.Session{}, models.Address{}, models.GlobalAttribute{},
		models.Website{}, models.Column{}, models.ClothMaterial{}, models.Category{}, models.User{}, models.OrderEvent{},
		models.UserRole{}, models.OrderRequest{}, models.OrderDetails{}, models.Design{}, models.PrintCategory{}, models.Quotation{},
	}

	context.Config.GormDb.AutoMigrate(mdls...)

	for _, model := range mdls {
		installModel(model)
	}
}
