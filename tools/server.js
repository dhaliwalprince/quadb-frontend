const http = require('http');
const url = require('url');
const fs = require('fs');
const path = require('path');
const httpProxy = require('http-proxy')
const port = process.argv[2] || 9000;

const apiServerUrl = 'http://localhost:8080'
const proxy = httpProxy.createProxyServer({ forward: apiServerUrl})
http.createServer(function (req, res) {
  console.log(`${req.method} ${req.url}`);

  if (req.url.startsWith('/api')) {
    // forwarding request to api server
    proxy.web(req, res, { target: apiServerUrl }, function(error) {
        console.log(error)
        res.statusCode = 500
        res.end(`Url ${url} doesn't exist.`)
        return;
    })
    return;
  }
  // parse URL
  const parsedUrl = url.parse(req.url);
  // extract URL path
  let pathname = `build${parsedUrl.pathname}`;
  // based on the URL path, extract the file extention. e.g. .js, .doc, ...
  let ext = path.parse(pathname).ext;
  // maps file extention to MIME typere
  const map = {
    '.ico': 'image/x-icon',
    '.html': 'text/html',
    '.js': 'text/javascript',
    '.json': 'application/json',
    '.css': 'text/css',
    '.png': 'image/png',
    '.jpg': 'image/jpeg',
    '.wav': 'audio/wav',
    '.mp3': 'audio/mpeg',
    '.svg': 'image/svg+xml',
    '.pdf': 'application/pdf',
    '.doc': 'application/msword'
  };

  if (ext.length === 0) {
      ext = '.html'
  }

  fs.exists(pathname, function (exist) {
    if(!exist) {
      proxy.web(req, res, { target: apiServerUrl }, function(error) {
          console.log(error)
          res.statusCode = 500
          res.end(`Url ${pathname} doesn't exist.`)
          return;
      })
      return;
    }

    // if is a directory search for index file matching the extention
    if (fs.statSync(pathname).isDirectory()) pathname += '/index' + ext;

    // read file from file system
    fs.readFile(pathname, function(err, data){
      if(err){
        res.statusCode = 500;
        res.end(`Error getting the file: ${err}.`);
      } else {
        // if the file is found, set Content-type and send data
        res.setHeader('Content-type', map[ext] || 'text/plain' );
        res.end(data);
      }
    });
  });


}).listen(parseInt(port));

console.log(`Server listening on port ${port}`);